import groovy.transform.TypeChecked
import globals

@TypeChecked
def call(Map parms) {
    // Project Definition Parameters
    String nodeLabel = required value: parms.get('nodeLabel'), message: "'nodeLabel' parameter not set in the Jenkinsfile.  This parameter is required."
    String versionFile = required value: parms.get('versionFile'), message: "'versionFile' parameter not set in the Jenkinsfile.  This parameter is required."

    // Process control
    Boolean installDependencies = parms.get("installDependencies", false)
    String installDependenciesCommand = parms.get("installDependenciesCommand", "")

    Boolean npmBuild = parms.get("npmBuild", false)
    Boolean dotnetBuild = parms.get("dotnetBuild", false)
    Boolean dotnetClean = parms.get("dotnetClean", false)
    Boolean dotnetClearNuget = parms.get("dotnetClearNuget", false)
    String buildCommand = parms.get("buildCommand", "")
    String publishFolder = parms.get("publishFolder", "")
    String publishFolderPathSegment = publishFolder ? publishFolder + '/' : ''
    Boolean updateVersions = parms.get("updateVersions", false)

    Boolean buildDocs = parms.get("buildDocs", false)

    Boolean npmLint = parms.get("npmLint", false)
    String lintCommand = parms.get("lintCommand", "")
    Boolean npmTest = parms.get("npmTest", false)
    String testCommand = parms.get("testCommand", "")
    Boolean npmCoverage = parms.get("npmCoverage", false)
    String coverageCommand = parms.get("coverageCommand", "")
    Boolean runNpmTests = npmLint || npmTest || npmCoverage
    String testCommandLine = "echo " +
        (npmLint ? "&& npm run lint${lintCommand}" : "") +
        (npmTest ? "&& npm run test${testCommand}" : "") +
        (npmCoverage ? "&& npm run coverage${coverageCommand}" : "")

    Boolean dotnetTest = parms.get("dotnetTest", false)
    String dotnetTestCommand = parms.get("dotnetTestCommand", "")
    Boolean dotnetPackage = parms.get("dotnetPackage", false)
    
    Boolean testReports = parms.get("testReports", false)
    Boolean coverageReports = parms.get("coverageReports", false)

    Boolean npmPackage = parms.get("npmPackage", false)
    Boolean nugetPackage = parms.get("nugetPackage", false)
    String packageCommand = parms.get("packageCommand", "")

    Boolean npmPublish = parms.get("npmPublish", false)
    Boolean nugetPublish = parms.get("nugetPublish", false)
    String publishCommand = parms.get("publishCommand", "")

    Boolean deployArtifacts = parms.get("deployArtifacts", false)
    String deployCommand = parms.get("deployCommand", "")

    // notify	(always executes): send emails to "notified" folks and send slack msg to slack channel
    String notified = parms.get("notified", "")

    // Define pipeline global variables.
    // TODO: Throw errors if required parms not set.
    String npmRepoName = parms.npmRepoName
    String npmScope = parms.npmScope
    Boolean npmClean = parms.get("npmClean", false)

    String reportFolder = parms.reportFolder
    String reportServerPhysicalRoot = "/var/www/reports/${reportFolder}"
    String reportServerPhysicalFolder
    Boolean runReports
    String reportsCommandLine

    String branchName
    String buildNumber

    String projectName = parms.get("projectName")
    String version
    String versionSuffixBase
    String nodeName
    String workspaceFolder
    String jobName
    String buildUrl
    String buildUrlSlackMessage
    String buildUrlHtmlMessage
    String octopusChannelId

    String dateBuildNumber
    String fullVersionNumber
    String reportUrl
    String notificationHtmlFooter = ''
    String notificationSlackFooter = ''

    String npmArtifactoryUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/npm/${npmRepoName}"
    String artifactoryPublishUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${parms.get('artifactoryPublishRepo', '')}/"
    
    String nupkgOctopusProjectId = parms.get("nupkgOctopusProjectId", "")
    String nupkgOctopusStepName = parms.get("nupkgOctopusStepName", "")
    String nupkgOctopusActionName = parms.get("nupkgOctopusActionName", "")
    String nupkgOctopusUnstableChannelId = parms.get("nupkgOctopusUnstableChannelId", "")
    String nupkgOctopusDefaultChannelId = parms.get("nupkgOctopusDefaultChannelId", "Default")

    String artifactoryZipFileBaseName = parms.get("artifactoryZipFileBaseName", "")
    String artifactorySourceFolder = parms.get("artifactorySourceFolder", "")
    String artifactoryZipFileName
    Boolean artifactoryZipDeploy = parms.get("artifactoryZipDeploy", false)   
    String artifactoryZipOctopusProjectId = parms.get("artifactoryZipOctopusProjectId", "")
    String artifactoryZipOctopusStepName = parms.get("artifactoryZipOctopusStepName", "")
    String artifactoryZipOctopusActionName = parms.get("artifactoryZipOctopusActionName", "")

    String artifactoryNugetRepo = parms.get("artifactoryNugetRepo", "")
    if (dotnetBuild && !artifactoryNugetRepo){
        error("'artifactoryNugetRepo' parameter not set when dotnet build has been enabled!")
    }
    String artifactoryNugetUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${artifactoryNugetRepo}/"

    Boolean nugetBuild = parms.get("nugetBuild", false)
    String nugetBaseFileName = parms.get("nugetBaseFileName", "")
    String nuspecBaseFileName = parms.get("nuspecBaseFileName", "")
    String nuspecFolder = parms.get("nuspecFolder", ".")
    String nuspecAuthor = parms.get("nuspecAuthor", "")
    String nuspecDescription = parms.get("nuspecDescription", "Placeholder for Nuspec Description")
    String nupkgFileName
    String nuspecFileName
    Boolean nugetDeploy = parms.get("nugetDeploy", false)

    String slackChannel = parms.slackChannel
    Boolean isPR = isPullRequest()
    Boolean isRel = isRelease()
   
pipeline {
    agent {
        node {
            label nodeLabel
            customWorkspace URLDecoder.decode("/home/jenkins/workspace/${JOB_NAME}", "UTF-8")
        }    
    }
    options {
        timeout(time: 1, unit: 'HOURS')
    }
    stages {
        // Initialize pipeline.
        // Environment has been set up by now, so it is safe to set variables from it.
        stage('init') {
        steps {
            script {
                try {
                    // Set proxies for dotnet.
                    env.http_proxy = "http://bcpxy.nycnet:8080"
                    env.https_proxy = "http://bcpxy.nycnet:8080"

                    // Get environment values set by Jenkins.
                    branchName = "${env.BRANCH_NAME}"
                    buildNumber = "${env.BUILD_NUMBER}"
                    jobName = "${env.JOB_NAME}"
                    nodeName = "${env.NODE_NAME}"
                    workspaceFolder = "${env.WORKSPACE}"
                    buildUrl = "${env.RUN_DISPLAY_URL}"
                    buildUrlSlackMessage = "Click <${buildUrl}|here> to view job details"
                    buildUrlHtmlMessage = "<p>Click <a href=\'${buildUrl}\'>here</a> to view job details.</p>"

                    if (npmClean) {
                        sh "rm -rf node_modules"
                    }

                    // Read the version from the version file.
                    version = readVersion versionFile
                    
                    // Generate unique date / build stamp for file names
                    dateBuildNumber = buildSerialNumber buildNumber

                    // Generate the full version number.
                    fullVersionNumber = generateVersionNumber version, branchName, dateBuildNumber

                    // Extract the version suffix.
                    versionSuffix = fullVersionNumber.substring(version.length());

                    // get the channelID
                    octopusChannelId = branchName =~ /^(feature\/|develop)/ ? "${nupkgOctopusUnstableChannelId}": "${nupkgOctopusDefaultChannelId}"
                    reportUrl = "reports/${reportFolder}/${dateBuildNumber}"    

                    String createFolderCommand = "sshpass -p '${globals.reportServerPassword}' ssh -p 22 ${globals.reportServerUser} mkdir -p ${reportServerPhysicalRoot}"
                    String publishTestReports = "&& sshpass -p '${globals.reportServerPassword}' rsync -r reports/ ${globals.reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"
                    String publishCoverageReports = "&& sshpass -p '${globals.reportServerPassword}' rsync -r coverage ${globals.reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"

                    runReports = testReports || coverageReports
                    reportsCommandLine = createFolderCommand +
                        (testReports ? publishTestReports : "") +
                        (coverageReports ? publishCoverageReports : "")

                    // If runReports is true but no reportFolder was specified, thow an error.
                    if (runReports && !reportFolder) {
                        error("runReports is true but no reportFolder was specified - please specify the reportFolder parameter.")
                        currentBuild.result = 'ABORTED'
                    }

                    notificationHtmlFooter = runReports? "<p>Reports are available at <a href=\'http://${globals.reportServer}/${reportUrl}/\'>View Reports</a></p>" : ""
                    notificationSlackFooter = runReports ? "\nReports are available at <http://${globals.reportServer}/${reportUrl}|View Reports>" : ""

                    artifactoryZipFileName = "${artifactoryZipFileBaseName}.${fullVersionNumber}.zip"

                    nupkgFileName = "${nugetBaseFileName}.${fullVersionNumber}.nupkg"
                    nuspecFileName = "${nuspecBaseFileName}.${fullVersionNumber}.nuspec"

                    // If the project uses npm, configure it for the job.
                    if (installDependencies || npmBuild || runNpmTests || npmPackage || npmPublish) {
                        echo "Configure npm to use Artifactory"
                        sh "npm config set ${npmScope}:registry https://${npmArtifactoryUrl}/"

                        sh "npm config set //${npmArtifactoryUrl}:_password ${globals.artifactoryNpmPassword}"
                        sh "npm config set //${npmArtifactoryUrl}:username ${globals.artifactoryUserName}"
                        sh "npm config set //${npmArtifactoryUrl}:email ${globals.artifactoryEmail}"
                        sh "npm config set //${npmArtifactoryUrl}:always-auth true"
                    }

                    }
                    catch (Exception e) {
                        echo e.toString()
                    }
                }

            echo "Running job with parameters:"
            echo """
artifactoryEmail: $globals.artifactoryEmail
artifactoryNugetUrl: $artifactoryNugetUrl
artifactoryNpmPassword: $globals.artifactoryNpmPassword
artifactoryPublishUrl: $artifactoryPublishUrl
artifactoryRoot: $globals.artifactoryRoot
artifactoryServerName: $globals.artifactoryServerName
artifactorySourceFolder: $artifactorySourceFolder
artifactoryUserName: $globals.artifactoryUserName
artifactoryZipDeploy: $artifactoryZipDeploy
artifactoryZipFileBaseName: $artifactoryZipFileBaseName
artifactoryZipFileName: $artifactoryZipFileName
artifactoryZipOctopusActionName: $artifactoryZipOctopusActionName
artifactoryZipOctopusProjectId: $artifactoryZipOctopusProjectId
artifactoryZipOctopusStepName: $artifactoryZipOctopusStepName
branchName: $branchName
buildCommand: $buildCommand
buildNumber: $buildNumber
buildUrl: $buildUrl
coverageCommand: $coverageCommand
coverageReports: $coverageReports
dateBuildNumber: $dateBuildNumber
deployArtifacts: $deployArtifacts
deployCommand: $deployCommand
dotnetBuild: $dotnetBuild
dotnetClearNuget: $dotnetClearNuget
dotnetClean: $dotnetClean
dotnetPackage: $dotnetPackage
dotnetTest: $dotnetTest
dotnetTestCommand: $dotnetTestCommand
fullVersionNumber: $fullVersionNumber
gitChecksum: ${env.GIT_COMMIT}
installDependencies: $installDependencies
installDependenciesCommand: $installDependenciesCommand
isChangeRequest: $isPR
jobName: $jobName
lintCommand: $lintCommand
nodeName: $nodeName
npmArtifactoryUrl: $npmArtifactoryUrl
npmBuild: $npmBuild
npmClean: $npmClean
npmCoverage: $npmCoverage
npmLint: $npmLint
npmPackage: $npmPackage
npmPublish: $npmPublish
npmRepoName: $npmRepoName
npmScope: $npmScope
npmTest: $npmTest
nugetBaseFileName: $nugetBaseFileName
nugetBuild: $nugetBuild
nugetDeploy: $nugetDeploy
nugetPackage: $nugetPackage
nugetPublish: $nugetPublish
nupkgFileName: $nupkgFileName
nupkgOctopusActionName: $nupkgOctopusActionName
nupkgOctopusProjectId: $nupkgOctopusProjectId
nupkgOctopusStepName: $nupkgOctopusStepName
nupkgOctopusUnstableChannelId: $nupkgOctopusUnstableChannelId
nupkgOctopusDefaultChannelId: $nupkgOctopusDefaultChannelId
nuspecAuthor: $nuspecAuthor
nuspecBaseFileName: $nuspecBaseFileName
nuspecDescription: $nuspecDescription
nuspecFileName: $nuspecFileName
nuspecFolder: $nuspecFolder
octopusApiKey: $globals.octopusApiKey
octopusUrl: $globals.octopusUrl
packageCommand: $packageCommand
projectName: $projectName
publishCommand: $publishCommand
publishFolder: $publishFolder
reportFolder: $reportFolder
reportsCommandLine: $reportsCommandLine
reportServer: $globals.reportServer
reportServerPassword: $globals.reportServerPassword
reportServerPhysicalFolder: $reportServerPhysicalFolder
reportServerPhysicalRoot: $reportServerPhysicalRoot
reportUrl: $reportUrl
runNpmTests: $runNpmTests
runReports: $runReports
slackChannel: $slackChannel
testCommand: $testCommand
testCommandLine: $testCommandLine
testReports: $testReports
version: $version
versionFile: $versionFile
versionSuffixBase: $versionSuffixBase
workspaceFolder: $workspaceFolder
"""
            echo "Init Stage Complete."
            }
        }
        // If installDependencies set, install dependencies from the Artifactory NPM registry.
        stage('install-dependencies') {
            when {
                equals expected: true, actual: installDependencies
            }
            steps {
                // Configure npm to use Artifactory for both retrieving npms *from* Artifactory (all app front-ends)
                // and also for pushing npms *to* Artifactory (CAF-Angular only)
                // TODO: Encapsulate duplicate copies of this code into a method.
                // First confirm that npm config settings persist for lifecycle of the Jenkins job
                sh "npm install ${installDependenciesCommand}"
                echo "npm install completed"
            }
        }
        // If npmBuild set, execute 'npm run' 
        stage('build-npm') {
            when {
                equals expected: true, actual: npmBuild
            }
            steps {
                // Build the application.
                sh "npm run ${buildCommand}"
            }
        }
        // If buildDocs set, build and deploy the documentation application (CAF-Angular only)
        stage('build-docs') {
            when {
                equals expected: true, actual: buildDocs
            }
            steps {
                script {                
                    echo "Writing nuspec file: ${nuspecFolder}/${nuspecFileName}"
                    writeNuspecFile nuspecFolder, nuspecFileName, nuspecBaseFileName, fullVersionNumber, nuspecAuthor, nuspecDescription
                }
            }
        }
        // If dotnetClearNuget set, execute dotnet nuget locals --clear all
        stage('dotnet-clear-nuget') {
            when {
                equals expected: true, actual: dotnetClearNuget
            }
            steps {
                // Build the application.
                sh "dotnet nuget locals --clear all"
            }
        }
        // Delete the bin / obj folders if dotnetClean set.
        stage('dotnet-clean') {
            when {
                equals expected: true, actual: dotnetClean
            }
            steps {
                echo 'Deleting bin and obj folders from previous builds.'
                sh "find . -iname 'bin' -o -iname 'obj' | xargs rm -rf"
            }
        }
        // If dotnetBuild set, build the dotnet project
        stage('build-dotnet') {
            when {
                equals expected: true, actual: dotnetBuild
            }
            steps {
                // Build the application.
                sh "dotnet clean"
                sh "dotnet restore --configfile ${workspaceFolder}/nuget.config --source https://${artifactoryNugetUrl}"
                sh "dotnet build --configuration Release --configfile ${workspaceFolder}/nuget.config --source https://${artifactoryNugetUrl}"
                sh "dotnet publish --configuration Release -o ${publishFolder} --configfile ${workspaceFolder}/nuget.config --source https://${artifactoryNugetUrl}"
                // Create the nuspec file and save it in the build output folder.
                script {
                    echo "Writing nuspec file: ${nuspecFolder}/${publishFolderPathSegment}${nuspecFileName}"
                    writeNuspecFile nuspecFolder, publishFolderPathSegment + nuspecFileName, nuspecBaseFileName, fullVersionNumber, nuspecAuthor, nuspecDescription

                    if (fileExists("${nuspecFolder}/${publishFolderPathSegment}${nuspecFileName}")) {
                        echo 'File written successfully.'
                    } else {
                        echo 'ERROR - File does not exist!'
                    }
                }
            }
        }
        // If nugetBuild set, write a nuspec file
        stage('build-nuget') {
            when {
                equals expected: true, actual: nugetBuild
            }
            steps {
                script {
                    echo "Writing nuspec file: ${nuspecFolder}/${publishFolderPathSegment}${nuspecFileName}"
                    writeNuspecFile nuspecFolder, publishFolderPathSegment + nuspecFileName, nuspecBaseFileName, fullVersionNumber, nuspecAuthor, nuspecDescription
                }
            }
        }
        // If updateVersions set, update the versions in all target files found in the project.
        stage('update-versions') {
            when {
                equals expected: true, actual: updateVersions
            }
            steps {
                echo "Versioning ${versionFile}"
                script {
                    updateProjectVersions "${buildNumber}${versionSuffixBase}"
                }
            }
        }
        // Run tests on the project.
        stage('npm-test') {
            when {
                equals expected: true, actual: runNpmTests
            }
            steps {
                sh "${testCommandLine}"
            }
        }
        stage('dotnet-test') {
            when {
                equals expected: true, actual: dotnetTest
            }
            steps {
                sh "dotnet test${dotnetTestCommand} --no-restore"
            }
        }
        // Copy test reports to the reports server.
        stage('copy-reports') {
            when {
                allOf {
                    equals expected: true, actual: runReports
                    equals expected: false, actual: isPR
                }
            }
            steps {
                sh reportsCommandLine
            }
        }
        // Should only run for applications
        stage('npm-package') {
            when {
                allOf {
                    equals expected: true, actual: npmPackage
                    equals expected: false, actual: isPR
                }
            }
            steps {
                sh "npm run package${packageCommand}"
            }
        }
        stage('dotnet-package') {
            when {
                allOf {
                    equals expected: true, actual: dotnetPackage
                    equals expected: false, actual: isPR
                }
            }
            steps {
                // Move to the nuspecFolder folder and create the nupkg file in the parent folder using zip.
                // The asterisk wildcard is placed in single-quoted string to avoid being interpreted as the Groovy spread operator.
                sh 'cd ' + nuspecFolder + (publishFolder ? '/' : '') + publishFolder + ' && zip -v -r ../../' + nupkgFileName + ' .' 
                //sh "cd ${nuspecFolder}/${publishFolder} && zip -r ../../${nupkgFileName} " + '*' 
            }
        }
        stage('nuget-package') {
            when {
                allOf {
                    equals expected: true, actual: nugetPackage
                    equals expected: false, actual: isPR
                }
            }
            steps {
                // Move to the nuspecFolder folder and create the nupkg file in the parent folder using zip.
                sh "cd ${nuspecFolder} && zip -r ../${nupkgFileName} ."
            }
        }
        stage('npm-publish') {
            when {
                allOf {
                    equals expected: true, actual: npmPublish
                    equals expected: false, actual: isPR
                }
            }
            steps {
                echo "Publishing npms to Artifactory"
                sh 'chmod -R 755 ./scripts'
                sh "npm publish --registry https://${npmArtifactoryUrl} ${publishCommand}"
            }
        }
        stage('nuget-publish') {
            when {
                allOf {
                    equals expected: true, actual: nugetPublish
                    equals expected: false, actual: isPR
                }
            }
            steps {
                echo "Publish zip to Artifactory"
                sh "dotnet nuget push ${nupkgFileName} --api-key ${globals.artifactoryUserName}:${globals.artifactoryNugetPassword} --source https://${artifactoryPublishUrl}"
            }
        }
        stage('prepare-artifacts-folder') {
            when {
                allOf {
                    equals expected: true, actual: deployArtifacts
                    equals expected: false, actual: isPR
                }
            }
            steps {
                echo "Zipping folder to push to Artifactory"
                sh "zip -r ${artifactoryZipFileName} ${artifactorySourceFolder}"
            }
        }
        stage('artifactory-zip-deploy') {
            when {
                allOf {
                    equals expected: true, actual: artifactoryZipDeploy
                    equals expected: false, actual: isPR
                }
            }
            steps {
                //echo "transfer the file to artifactory at the specified URL"
                sh "curl --proxy http://bcpxy.nycnet:8080 -u ${globals.artifactoryUserName}:${globals.artifactoryNpmPassword} -T ${artifactoryZipFileName} https://${npmArtifactoryUrl}/builds/${projectName}/${dateBuildNumber}"
            
                echo "Creating new Octopus Release which will trigger auto-deploy"
                // Create the JSON file and save it in the current folder.
                createOctopusRelease(
                    projectId: artifactoryZipOctopusProjectId, 
                    version: fullVersionNumber, 
                    stepName: artifactoryZipOctopusStepName, 
                    actionName: artifactoryZipOctopusActionName, 
                    octopusChannelId: octopusChannelId,
                    buildUrl: buildUrl
                )
            }
        }
        stage('nuget-deploy') {
            when {
                allOf {
                    equals expected: true, actual: nugetDeploy
                    equals expected: false, actual: isPR
                }
            }
            steps {           
                echo "Creating new Octopus Release which will trigger auto-deploy"
                // Create the JSON file and save it in the current folder.
                createOctopusRelease(
                    projectId: nupkgOctopusProjectId, 
                    version: fullVersionNumber, 
                    stepName: nupkgOctopusStepName, 
                    actionName: nupkgOctopusActionName, 
                    octopusChannelId: octopusChannelId,
                    buildUrl: buildUrl
                )
           }
        }
    }
    post {
            // Send notifications after the job completes.
            success {
                emailext (
                recipientProviders: [[$class: 'DevelopersRecipientProvider']],
                to: "${notified}",
                mimeType: 'text/html',
                subject: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                slackSend channel: slackChannel, color: "#00FF00", message: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}${notificationSlackFooter}"
            }
            failure {
                emailext (
                recipientProviders: [[$class: 'CulpritsRecipientProvider']],
                to: "${notified}",
                mimeType: 'text/html',
                subject: "ERROR: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>ERROR: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                slackSend channel: slackChannel, color: "#FF0000", message: "ERROR: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}${notificationSlackFooter}"
            }
        }
    }
}
import hudson.model.*

class cafConfig {

    public static final String artifactoryRoot = "caf.jfrog.io"
    public static final String artifactoryServerName = "caf"
    public static final String octopusUrl = "10.243.44.200"
    public static final String octopusApiKey = "API-O6U8OQ8PKAYVW1JT8HMJQ0RRD2E"

    private String _repoName
    private String _artifactoryNpmScope

    private String _reportFolder
    private String _reportServer
    private String _reportServerPassword

    private String _branchName
    private String _changeBranch
    private String _changeTarget
    private String _buildId
    private String _buildNumber
    private String _gitBranch

    private String _projectName
    private String _version
    private String _versionSuffix

    private String _dateBuildNumber
    private String _fullVersionNumber
    private String _reportUrl
    private String _notificationFooter

    private Boolean _execute
    
    public cafConfig() {

    }

    public cafConfig(
        String packageJson,
        String repoName,
        String artifactoryNpmScope,
        String reportFolder,
        String reportServer,
        String reportServerPassword,
        String branchName,
        String changeBranch,
        String changeTarget,
        String buildId,
        String buildNumber,
        String gitBranch,
        Boolean execute
        ) {

        _repoName = repoName
        _artifactoryNpmScope = artifactoryNpmScope

        _reportFolder = reportFolder
        _reportServer = reportServer
        _reportServerPassword = reportServerPassword

        _branchName = branchName
        _changeBranch = changeBranch
        _changeTarget = changeTarget
        _buildNumber = buildNumber
        _buildId = buildId
        _gitBranch = gitBranch

        _execute = execute

        String curDir = sh(returnStdout: true, script: 'pwd').trim()
        echo curDir
        String packageJsonText = new File("$curDir/$packageJson").text
        def rxProjectName = packageJsonText =~ /"name": "([^"]+)"/
        def rxVersion = packageJsonText =~ /"version": "([^"]+)"/

        _projectName = rxProjectName[0][1]
        _version = rxVersion[0][1]

        // Determine file name suffix, if any.
        _versionSuffix = _branchName =~ /^(feature\/|develop)/ ? '-unstable' : _branchName =~ /^hotfix\// ? '-hotfix': ''

        Calendar cal = new GregorianCalendar()

        // Generate unique date / build stamp for file names
        _dateBuildNumber = "${cal.get(Calendar.YEAR)}${cal.get(Calendar.MONTH)}${cal.get(Calendar.DAY_OF_MONTH)}-${_buildNumber}"
        _fullVersionNumber = "${_version()}-${_buildNumber}${_versionSuffix}"
        _reportUrl = "reports/${_reportFolder}/${_dateBuildNumber}"
        _notificationFooter = ""

        // def env = System.getenv()
    /*
        def newParams = null

        def pl = new ArrayList<StringParameterValue>()
        pl.add(new StringParameterValue('ARTIFACTORY_ROOT', artifactoryRoot))

        def oldParams = build.getAction(ParametersAction.class)

        if (oldParams != null) {
            newParams = oldParams.createUpdated(pl)
            build.actions.remove(oldParams)
        } else {
           newParams = new ParametersAction(pl)
        }

        build.addAction(newParams)
        // env["ARTIFACTORY_ROOT"] = artifactoryRoot
        */
    }

    String getProjectName() {
        "$_projectName"
    }

    String getBuildNumber() {
        "$_buildNumber"
    }

    String getDateBuildNumber() {
        "$_dateBuildNumber"
    }

    String getVersion() {
        "$_version"
    }

    String getVersionSuffix() {
        "$_versionSuffix"
    }

    String getFullVersionNumber() {
        "$_fullVersionNumber"
    }

    String getReportUrl() {
        "$_reportUrl"
    }

    String npmArtifactoryUrl() {
        "$artifactoryRoot/$artifactoryServerName/api/npm/$_repoName"
    }

    String toString() {
        """
        artifactoryRoot: $artifactoryRoot
        artifactoryServerName: $artifactoryServerName
        artifactory Repo Name: $_repoName
        octopusUrl: $octopusUrl

        repo name: $_repoName
        artifactory npm scope: $_artifactoryNpmScope

        report folder: $_reportFolder
        report server: $_reportServer

        branch name: $_branchName
        change branch: $_changeBranch
        change target: $_changeTarget
        build id: $_buildId
        build number: $_buildNumber
        git branch: $_gitBranch

        project name: $_projectName
        version: $_version
        version suffix: $_versionSuffix

        date build number: $_dateBuildNumber
        full version number: $_fullVersionNumber
        report url: $_reportUrl
    """
    }
}

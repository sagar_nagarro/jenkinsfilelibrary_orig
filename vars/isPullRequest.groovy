// Function that report whether a build is based on a Pull Request or not.
// Unfortunately, the Jenkins changeRequest() method appears to report 'true' for non-PR builds!
// However, for PRs, branchName always starts with PR-\d+
// So be sure to not name branches that way...
def call() {
    def rxPR = env.BRANCH_NAME =~ /^PR-\d+$/

    return !!rxPR    
}

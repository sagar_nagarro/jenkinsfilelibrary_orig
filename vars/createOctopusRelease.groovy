import globals

// Function that writes a Nuspec file.
def call(Map parms) {
    writeFile file: "octopus.payload.json", text: """{
        "ProjectId": "${parms.projectId}",
        "ChannelId": "${parms.octopusChannelId}",
        "Version": "${parms.version}",
        "ReleaseNotes": "${parms.buildUrl}",
        "SelectedPackages": [
        {
            "StepName": "${parms.stepName}",
            "ActionName": "${parms.actionName}",
            "Version": "${parms.version}"
        }
        ]
    }"""

    // POST JSON file to Octopus using the Octopus REST API.
    sh "curl -X POST http://${globals.octopusUrl}/api/releases -H 'Content-Type: application/json' -H 'X-Octopus-ApiKey: ${globals.octopusApiKey}' --data-binary @octopus.payload.json --trace-ascii octopus.log"
}
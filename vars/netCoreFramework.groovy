// This Jenkinsfile library is for CI/CD of Frameworks built with .NET Core 2.0.
import groovy.transform.TypeChecked
import globals

@TypeChecked
def call(Map parms) {
    // Project Definition Parameters

    // nodeLabel controls which Jenkins slaves this project can build on, and is required.
    String nodeLabel = required value: parms.get('nodeLabel'), message: "'nodeLabel' parameter not set in the Jenkinsfile.  This parameter is required."
    // projectName specifies a string that uniquely identifies the project, and is used to build several output filenames.  This is required.
    String projectName = required value: parms.get('projectName'), message: "'projectName' parameter not set in the Jenkinsfile.  This parameter is required."
    
    // projectFile specifies a project file to be passed as a parameter to the build command, and is required when the main project folder contains more than one project or solution file.
    String projectFile = parms.get("projectFile", '')

    // artifactoryPublishRepo specifies the folder within Artifactory to deploy the build package to, and is required.
    String artifactoryPublishRepo = required value: parms.get('artifactoryPublishRepo'), message: "Parameter 'artifactoryPublishRepo' was not defined - this is required to indicate the publish destination in Artifactory."
    String artifactoryPublishUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${artifactoryPublishRepo}/"

    // artifactoryNugetRepo specifies the folder within Artifactory to source Nuget packages from.
    String artifactoryNugetRepo = parms.get("artifactoryNugetRepo", globals.artifactoryNugetRepo)
    String artifactoryNugetUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${artifactoryNugetRepo}"

    // Process Control Parameters (can be set to false to skip for faster builds)
    // init (always executes): - Setup variables and print out values for debugging
    String initCommand = parms.get("initCommand", "")

    // install-dependencies-nuget:	configure artifactory for pull and push of nuget, nuget install
    Boolean installDependenciesNuget = parms.get("installDependenciesNuget", false)

    Boolean buildDotnet = parms.get("buildDotnet", false)
    String buildDotnetCommand = parms.get("buildDotnetCommand", "")
    Boolean cleanDotnet = parms.get("cleanDotnet", false)
    Boolean clearNuget = parms.get("clearNuget", false)

    Boolean publishDotnet = parms.get("publishDotnet", false)

    // test-dotnet:	run dotnet unit tests and feature tests and generates the reports
    Boolean testDotnet = parms.get("testDotnet", false)
    String testDotnetCommand = " " + parms.get("testDotnetCommand", "")
    String testProjectFolder = parms.get("testProjectFolder", '')
    
    // copy-reports-dotnet:	copy all test reports to report server
    String reportFolder = parms.get("reportFolder", "")
    String reportServerPhysicalRoot = "${globals.reportServerPhysicalRoot}/${reportFolder}"
    Boolean coverageReports = parms.get("coverageReports", false)
    String coverageDirectory = parms.get("coverageDirectory", "")
    Boolean runReports
    String reportsCommandLine
    String reportUrl
    def reportsMap = [:]

    String nugetConfig

    String branchName
    String buildNumber

    String versionSuffix
    String nodeName
    String workspaceFolder
    String jobName
    String buildUrl
    String buildUrlSlackMessage
    String buildUrlHtmlMessage

    String dateBuildNumber
    String fullVersionNumber = ""
    String notificationHtmlFooter = ''
    String notificationSlackFooter = ''

    String slackChannel = parms.slackChannel
    String notified = parms.get("notified", "")
    Boolean cleanWorkspaceAfterRun = parms.get("cleanWorkspaceAfterRun", false)
    Boolean isPR = isPullRequest()
    Boolean isRel = isRelease()
   
pipeline {
    agent {
        node {
            label nodeLabel
            customWorkspace URLDecoder.decode("/home/jenkins/workspace/${JOB_NAME}", "UTF-8")
        }    
    }
    options {
        timeout(time: 1, unit: 'HOURS')
    }
    stages {
        // Initialize pipeline.
        // Environment has been set up by now, so it is safe to set variables from it.
        stage('init') {
            steps {
                script {
                    Boolean abortedMasterBuild = false

                    try {
                        // Set proxies in the environment for dotnet.
                        env.http_proxy = globals.http_proxy
                        env.https_proxy = globals.https_proxy

                        // Execute any initCommand defined for the job.
                        if (initCommand != '') {
                            sh initCommand
                        }

                        // Get environment values set by Jenkins.
                        branchName = "${env.BRANCH_NAME}"
                        buildNumber = "${env.BUILD_NUMBER}"
                        jobName = "${env.JOB_NAME}"
                        nodeName = "${env.NODE_NAME}"
                        workspaceFolder = "${env.WORKSPACE}"
                        buildUrl = "${env.RUN_DISPLAY_URL}"
                        buildUrlSlackMessage = "Click <${buildUrl}|here> to view job details"
                        buildUrlHtmlMessage = "<p>Click <a href=\'${buildUrl}\'>here</a> to view job details.</p>"

                        // Prevent builds against master.
                        if (branchName == 'master') {
                            abortedMasterBuild = true
                            echo "Attempt to build from MASTER branch - aborting..."
                            error("Attempt to build from MASTER branch - aborting...")
                        }

                        // Determine version suffix, if any.
                        String versionSuffixBase = getVersionSuffixBase branchName

                        if (isPR) {
                            versionSuffixBase = ".${versionSuffixBase}"
                        }

                        // Generate unique date / build stamp for file names
                        dateBuildNumber = buildSerialNumber buildNumber

                        // Calculate the version suffix.  If we are doing a PR, ignore it as we will not deploy.
                        versionSuffix = (isPR | isRel) ? "" : "${versionSuffixBase}.${dateBuildNumber}"

                        // If projectFile is specified (not using default blank if there is only one target in the folder)
                        // verify that it exists.
                        if (projectFile != '') {       
                            Boolean exists = fileExists "${workspaceFolder}/${projectFile}"

                            if (!exists) {
                                error "The project file to build '${projectFile} does not exist!'"
                            }
                        }

                        // Update project version numbers.
                        if (versionSuffix != '') {
                            updateProjectVersions versionSuffix
                        }

                        // Configure reporting variables
                        String reportServerReportPhysicalRoot = "${reportServerPhysicalRoot}/${fullVersionNumber}"
                        String createReportFolderCommand = "sshpass -p '${globals.reportServerPassword}' ssh -p 22 ${globals.reportServerUser} mkdir -p ${reportServerReportPhysicalRoot}/unit-tests"
                        String publishTestReports = "&& sshpass -p '${globals.reportServerPassword}' rsync ${workspaceFolder}/reports-index.html ${globals.reportServerUser}:${reportServerReportPhysicalRoot}/index.html" +
                            "&& sshpass -p '${globals.reportServerPassword}' rsync ${workspaceFolder}/unit-tests-index.html ${globals.reportServerUser}:${reportServerReportPhysicalRoot}/unit-tests/index.html"
                        String publishCoverageReports = "&& dotnet reportgenerator \"-reports:${workspaceFolder}/coverage.xml\" \"-targetdir:${workspaceFolder}/coverage\" " + 
                            "&& sshpass -p '${globals.reportServerPassword}' rsync -r ${workspaceFolder}/coverage ${globals.reportServerUser}:${reportServerReportPhysicalRoot}"

                        if (testProjectFolder != '') {
                            publishCoverageReports = "&& cd ${workspaceFolder}/${testProjectFolder}" +
                                publishCoverageReports +
                                "&& cd ${workspaceFolder}"
                        }

                        runReports = testDotnet || coverageReports
                        reportsCommandLine = createReportFolderCommand +
                            (testDotnet ? publishTestReports : "") +
                            (coverageReports ? publishCoverageReports : "")

                        if (testDotnet) {
                            testDotnetCommand += " --logger 'trx;LogFileName=../../results.trx'"
                            reportsMap['Unit Test Report'] = 'unit-tests/index.html'
                        }

                        if (coverageReports) {
                            testDotnetCommand += " /p:CollectCoverage=true  /p:CoverletOutputFormat=opencover"
                            reportsMap['Code Coverage Report'] = 'coverage/index.htm'

                            if (coverageDirectory != '') {
                                testDotnetCommand += " /p:CoverletOutputDirectory='${coverageDirectory}'"
                            }
                        }

                        // If runReports is true but no reportFolder was specified, thow an error.
                        if (runReports && !reportFolder) {
                            error("runReports is true but no reportFolder was specified - please specify the reportFolder parameter.")
                            currentBuild.result = 'ABORTED'
                        }
                        reportUrl = "reports/${reportFolder}/${fullVersionNumber}" 
 
                        notificationHtmlFooter = "<p>Reports are available at <a href=\'http://${globals.reportServer}/${reportUrl}/\'>View Reports</a></p>"
                        notificationSlackFooter = "\nReports are available at <http://${globals.reportServer}/${reportUrl}|View Reports>"
                        // / Reporting variables   

                        // Send build start notification.
                        sendBuildStartNotification notified, jobName, buildNumber, nodeName, buildUrl
                    }
                    catch (Exception e) {
                        // Check for abort due to master build attempt.
                        if (abortedMasterBuild) {
                            currentBuild.result = 'ABORTED'
                            throw e
                            // return here instead of throwing error to keep the build "green"
                            // return
                        }

                        echo e.toString()
                    }
                }
                echo "Init Stage Complete."
            } // /steps
        } // /stage
        stage('show-job-parameters') {
            steps {               
                echo """
branchName: $branchName
buildDotnet: $buildDotnet
buildDotnetCommand: $buildDotnetCommand
buildNumber: $buildNumber
buildUrl: $buildUrl
buildUrlHtmlMessage: $buildUrlHtmlMessage
cleanDotnet: $cleanDotnet
cleanWorkspaceAfterRun: $cleanWorkspaceAfterRun
clearNuget: $clearNuget
dateBuildNumber: $dateBuildNumber
fullVersionNumber: $fullVersionNumber
gitChecksum: ${env.GIT_COMMIT}
installDependenciesNuget: $installDependenciesNuget
isChangeRequest: $isPR
isRelease: $isRel
jobName: $jobName
nodeLabel: $nodeLabel
nodeName: $nodeName
projectFile: $projectFile
projectName: $projectName
publishDotnet: $publishDotnet
reportFolder: $reportFolder
reportsCommandLine: $reportsCommandLine
reportServerPhysicalRoot: $reportServerPhysicalRoot
reportUrl: $reportUrl
runReports: $runReports
slackChannel: $slackChannel
testDotnet: $testDotnet
testDotnetCommand: $testDotnetCommand
versionSuffix: $versionSuffix
workspaceFolder: $workspaceFolder
"""
            } // /steps
        } // /stage
        // install-dependencies-nuget:	configure artifactory for pull and push of nuget, nuget install
        stage('install-dependencies-nuget') {
            when {
                equals expected: true, actual: installDependenciesNuget
            }
            steps {
                script {                   
                    // Create CI/CD nuget.config file.'
                    echo "Deleting any existing nuget.config files"
                    deleteNugetConfig
                    nugetConfig = "${workspaceFolder}/nuget.config"
                    echo "Writing Nuget config file"
                    writeNugetConfig nugetConfig, "${env.https_proxy}", "${artifactoryNugetUrl}"
                }
            }
        }
        // If buildDotnet set, build the dotnet project
        stage('build-dotnet') {
            when {
                equals expected: true, actual: buildDotnet
            }
            steps {                
                // Update project version numbers.
                script {
                    if (cleanDotnet) {
                        echo 'Deleting bin and obj folders from previous builds.'
                        sh "find . -iname 'bin' -o -iname 'obj' | xargs rm -rf"                        
                    }

                    if (clearNuget) {
                        echo "Clearing nuget packages"
                        sh "dotnet nuget locals --clear all"                       
                    }
                }

                // Fix up slashes as per https://github.com/Microsoft/msbuild/issues/1957
                sh "sed -i 's#\\\\#/#g' ${projectFile}"
                // Build the project.
                echo "Building project ${projectFile} with config file ${nugetConfig}"
                sh "dotnet build ${projectFile} ${buildDotnetCommand} --configfile ${nugetConfig} --source https://${artifactoryNugetUrl}"
                sh "dotnet pack ${projectFile} --no-build --include-symbols --include-source"
            }
        }
        stage('test-dotnet') {
            when {
                equals expected: true, actual: testDotnet
            }
            steps {
                // Download trxer from Artifactory.
                sh "curl  --proxy ${https_proxy} -u${globals.artifactoryUserName}:${globals.artifactoryClearTextPassword} -otrxercore.1.0.0.nupkg  https://${artifactoryNugetUrl}/trxercore.1.0.0.nupkg"
                // Unzip trxer.
                sh 'unzip -j trxercore.1.0.0.nupkg lib/netcoreapp2.0/*'
                // Execute unit tests.
                sh "dotnet test${testDotnetCommand}"
                // Execute trxer.
                sh "dotnet trxercore.dll results.trx unit-tests-index.html"
            }
        }
        // Copy all test reports to the reports server.
        stage('copy-reports-dotnet') {
            when {
                allOf {
                    equals expected: true, actual: runReports
                    equals expected: false, actual: isPR
                }
            }
            steps {
                // Write the index page for the Reports site folder.
                echo 'Writing reports index file'
                writeReportIndexFile 'reports-index.html', projectName, fullVersionNumber, reportsMap
                // Execute the copy reports commands.
                sh reportsCommandLine
            }
        }
        stage('publish-dotnet') {
            when {
                allOf {
                    equals expected: true, actual: publishDotnet
                    equals expected: false, actual: isPR
                    }
                }
            steps {
                echo "Publishing nupkg files to Artifactory"
                // Find and update nupkg files.
                script {
                    def nupkgFiles = findFiles glob: "**/*.nupkg"
                    echo "${nupkgFiles.length} nuget packages found"

                    nupkgFiles.each { nupkgFile ->
                    String nupkgFileName = nupkgFile.getPath()
                        echo "${nupkgFileName}"
                        sh "dotnet nuget push ${nupkgFileName} --api-key ${globals.artifactoryUserName}:${globals.artifactoryNugetPassword} --source https://${artifactoryPublishUrl}"
                    }
                }
            }
        }
        stage('cleanup') {
            when {
                equals expected: true, actual: cleanWorkspaceAfterRun
            }
            steps {
                cleanWs()
            }
        }
    } // /stages
    post {
            // Send notifications after the job completes.
            success {
                emailext (
                recipientProviders: [[$class: 'DevelopersRecipientProvider']],
                    to: "${notified}",
                mimeType: 'text/html',
                subject: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}"
                )
                slackSend channel: slackChannel, color: "#00FF00", message: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}"
            }
            failure {
                emailext (
                recipientProviders: [[$class: 'CulpritsRecipientProvider']],
                    to: "${notified}",
                mimeType: 'text/html',
                subject: "ERROR: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>ERROR: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}"
                )
                slackSend channel: slackChannel, color: "#FF0000", message: "ERROR: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}"
            }
        }
    }
}
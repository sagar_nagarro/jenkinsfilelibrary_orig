// Function that derives the version suffix base from the branch name.
def call(String branchName) {
    String versionSuffixBase = branchName =~ /^(feature\/|develop)/ ? '-unstable' : branchName =~ /^hotfix\// ? '-hotfix': ''
    echo "Calculating version suffix base ${versionSuffixBase} for branch ${branchName}"
    return versionSuffixBase    
}

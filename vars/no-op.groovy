// This Jenkinsfile library does nothing but echo the label and Jenkins settings.  
// It only exists to satisfy the Jenkins requirement of the existence of a Jenkinsfile.
import globals

def call(Map parms) {

    String nodeLabel = required value: parms.get('nodeLabel'), message: "'nodeLabel' parameter not set in the Jenkinsfile.  This parameter is required."
    String projectName = required value: parms.get('projectName'), message: "'projectName' parameter not set in the Jenkinsfile.  This parameter is required."
      
pipeline {
    agent {
        node {
        label parms.label
        }    
    }
    stages {
        stage('init') {
        steps {
            script {
                try {
                    // Get environment values set by Jenkins.
                    branchName = "${env.BRANCH_NAME}"
                    buildNumber = "${env.BUILD_NUMBER}"
                    changeBranch = "${env.CHANGE_BRANCH}"
                    changeTarget = "${env.CHANGE_TARGET}"
                    gitBranch = "${env.GIT_BRANCH}"
                    jobName = "${env.JOB_NAME}"
                    nodeName = "${env.NODE_NAME}"
                    workspaceFolder = "${env.WORKSPACE}"
                    }
                    catch (Exception e) {
                        echo e.toString()
                    }
                }

            echo "Running job with parameters:"
            echo """
branchName: $branchName
buildNumber: $buildNumber
changeBranch: $changeBranch
changeTarget: $changeTarget
gitBranch: $gitBranch
jobName: $jobName
label: $label
nodeName: $nodeName
projectName: $projectName
workspaceFolder: $workspaceFolder
"""
                }
            }
        }
    }
}

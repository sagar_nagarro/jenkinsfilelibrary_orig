// Function that generates the full verison number.
def call(String version, String branchName, String dateBuildNumber) {
    // Feature branch builds, hotfixes and pull requests (which are not deployed)
    // have a string suffix appended to the version number.
    // Release builds do not, as they presume that the developer has
    // manually updated the version with 'rc#' if necessary.
    String versionSuffix = 
        branchName =~ /^(feature\/|develop)/ ? '-unstable' : 
        branchName =~ /^hotfix\// ? '-hotfix': 
        branchName =~ /^PR-\d+$/ ? '-pr' :
        ''

    // If the build is getting a string suffix, then the unique 
    // build serial number is added to it.
    if (versionSuffix != '') {
        versionSuffix += ".${dateBuildNumber}"
    }

    String fullVersionNumber = "${version}${versionSuffix}"
    return fullVersionNumber    
}

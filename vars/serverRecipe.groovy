// This Jenkinsfile library is for CI/CD of Server Applications built with .NET Core 2.0.
import groovy.transform.TypeChecked
import globals
 
@TypeChecked
def call(Map parms) {
    // Project Definition Parameters

    // nodeLabel controls which Jenkins slaves this project can build on, and is required.
    String nodeLabel = required value: parms.get('nodeLabel'), message: "'nodeLabel' parameter not set in the Jenkinsfile.  This parameter is required."

    // versionFile specifies a file that contains the version number that will be used for the entire project package, and is required.
    String versionFile = required value: parms.get('versionFile'), message: "'versionFile' parameter not set in the Jenkinsfile.  This parameter is required."

    // projectName specifies a string that uniquely identifies the project, and is used to build several output filenames.  This is required.
    String projectName = required value: parms.get('projectName'), message: "'projectName' parameter not set in the Jenkinsfile.  This parameter is required."
    
    // projectFile specifies a project file to be passed as a parameter to the build command, and is required when the main project folder contains more than one project or solution file.
    String projectFile = parms.get("projectFile", '')

    // artifactoryPublishRepo specifies the folder within Artifactory to deploy the build package to, and is required.
    String artifactoryPublishRepo = required value: parms.get('artifactoryPublishRepo'), message: "Parameter 'artifactoryPublishRepo' was not defined - this is required to indicate the publish destination in Artifactory."
    String artifactoryPublishUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${artifactoryPublishRepo}/"

    // artifactoryNugetRepo specifies the folder within Artifactory to source Nuget packages from.
    String artifactoryNugetRepo = parms.get("artifactoryNugetRepo", globals.artifactoryNugetRepo)
    String artifactoryNugetUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${artifactoryNugetRepo}"

    // Process Control Parameters (can be set to false to skip for faster builds)
    // init (always executes): - Setup variables and print out values for debugging
    String initCommand = parms.get("initCommand", "")

    // install-dependencies-nuget:	configure artifactory for pull and push of nuget, nuget install
    Boolean installDependenciesNuget = parms.get("installDependenciesNuget", false)

    Boolean buildDotnet = parms.get("buildDotnet", false)
    String buildDotnetCommand = parms.get("buildDotnetCommand", "")
    Boolean cleanDotnet = parms.get("cleanDotnet", false)
    Boolean clearNuget = parms.get("clearNuget", false)

    // test-dotnet:	run dotnet unit tests and feature tests and generates the reports
    Boolean testDotnet = parms.get("testDotnet","" )
    String testDotnetCommand = " " + parms.get("testDotnetCommand", "")
    String testProjectFolder = parms.get("testProjectFolder", '')
    
    // copy-reports-dotnet:	copy all test reports to report server
    String reportFolder = parms.get("reportFolder", "")
    String reportServerPhysicalRoot = "${globals.reportServerPhysicalRoot}/${reportFolder}"
    Boolean coverageReports = parms.get("coverageReports","" )
    String coverageDirectory = parms.get("coverageDirectory", "")
    Boolean runReports
    String reportsCommandLine
    String reportUrl
    def reportsMap = [:]

    // notify	(always executes): send emails to "notified" folks and send slack msg to slack channel
    String notified = parms.get("notified", "")

    Boolean cleanWorkspaceAfterRun = parms.get("cleanWorkspaceAfterRun", false)

    // init Phase 1 (always executes): - Setup variables and print out values for debugging
    String publishFolder = parms.get("publishFolder", "publish")
    String publishPhysicalFolder
    //String publishFolderPathSegment = publishFolder ? publishFolder + '/' : ''

    Boolean dotnetPackage = parms.get("dotnetPackage", false)
    String nupkgFileName
    String nuspecFileName
    String nugetConfig
    String nuspecId = parms.get("nuspecId", projectName)
    String nuspecAuthor = parms.get("nuspecAuthor", "")
    String nuspecDescription = parms.get("nuspecDescription", "Placeholder for Nuspec Description")

    Boolean distNuget = parms.get("distNuget", false)

    String branchName
    String buildNumber

    String version
    String versionSuffix

    String nodeName

    String workspaceFolder

    String jobName
    String buildUrl
    String buildUrlSlackMessage
    String buildUrlHtmlMessage
    String octopusChannelId

    String dateBuildNumber
    String fullVersionNumber = ""
    String notificationHtmlFooter = ''
    String notificationSlackFooter = ''
    String beforePublishCommand = parms.get("beforePublishCommand", "")
    
    String nupkgOctopusProjectId = parms.get("nupkgOctopusProjectId", "")
    String nupkgOctopusStepName = parms.get("nupkgOctopusStepName", "")
    String nupkgOctopusActionName = parms.get("nupkgOctopusActionName", "")
    String nupkgOctopusUnstableChannelId = parms.get("nupkgOctopusUnstableChannelId", "")
    String nupkgOctopusDefaultChannelId = parms.get("nupkgOctopusDefaultChannelId", "Default")

    String artifactoryZipOctopusProjectId = parms.get("artifactoryZipOctopusProjectId", "")
    String artifactoryZipOctopusStepName = parms.get("artifactoryZipOctopusStepName", "")
    String artifactoryZipOctopusActionName = parms.get("artifactoryZipOctopusActionName", "")

    Boolean deployNuget = parms.get("deployNuget", false)

    String slackChannel = parms.slackChannel
    Boolean isPR = isPullRequest()
    Boolean isRel = isRelease()
    
pipeline {
    agent any
    stages {
        // init Phase 2 (always executes): - Setup variables and print out values for debugging
        stage('init') {
            steps {
                echo "init - Setup variables and print out values for debugging"
                script {
                    Boolean abortedMasterBuild = false

                    try {
                        // Set proxies in the environment for dotnet.
                        env.http_proxy = globals.http_proxy
                        env.https_proxy = globals.https_proxy

                        // Execute any initCommand defined for the job.
                        if (initCommand != '') {
                            sh initCommand
                        }

                        // Get environment values set by Jenkins.
                        branchName = "${env.BRANCH_NAME}"
                        buildNumber = "${env.BUILD_NUMBER}"
                        jobName = "${env.JOB_NAME}"
                        nodeName = "${env.NODE_NAME}"
                        workspaceFolder = "${env.WORKSPACE}"
                        buildUrl = "${env.RUN_DISPLAY_URL}"
                        buildUrlSlackMessage = "Click <${buildUrl}|here> to view job details"
                        buildUrlHtmlMessage = "<p>Click <a href=\'${buildUrl}\'>here</a> to view job details.</p>"

                        // Prevent builds against master.
                        if (branchName == 'master') {
                            abortedMasterBuild = true
                            echo "Attempt to build from MASTER branch - aborting..."
                            error("Attempt to build from MASTER branch - aborting...")
                        }

                        // Read the project version from the version file.
                        version = readVersion versionFile
                       
                        // Generate unique date / build stamp for file names
                        dateBuildNumber = buildSerialNumber buildNumber

                        // Generate the full version number.
                        fullVersionNumber = generateVersionNumber version, branchName, dateBuildNumber

                        // Extract the version suffix.
                        versionSuffix = fullVersionNumber.substring(version.length());
                        
                        nugetConfig = "${workspaceFolder}/nuget.config"

                        // If projectFile is specified (not using default blank if there is only one target in the folder)
                        // verify that it exists.
                        if (projectFile != '') {       
                            Boolean exists = fileExists "${workspaceFolder}/${projectFile}"

                            if (!exists) {
                                error "The project file to build '${projectFile} does not exist!'"
                            }
                        }

                        // Define publish folder path.
                        publishPhysicalFolder = "${workspaceFolder}/${publishFolder}"

                        // Delete existing publish folder.
                        echo "Deleting any existing publish folder ${publishPhysicalFolder}"
                        sh "rm -rf ${publishPhysicalFolder}"

                        nupkgFileName = "${projectName}.${fullVersionNumber}.nupkg"
                        nuspecFileName = "${projectName}.${fullVersionNumber}.nuspec"

                        // get the channelID
                        octopusChannelId = branchName =~ /^(feature\/|develop)/ ? "${nupkgOctopusUnstableChannelId}": "${nupkgOctopusDefaultChannelId}"

                        // Configure build
                        buildDotnetCommand += " /p:NoPackageAnalysis=true"

                        // Configure reporting variables
                        String reportServerReportPhysicalRoot = "${reportServerPhysicalRoot}/${fullVersionNumber}"
                        String createReportFolderCommand = "sshpass -p '${globals.reportServerPassword}' ssh -o StrictHostKeyChecking=no -p 22 ${globals.reportServerUser} mkdir -p ${reportServerReportPhysicalRoot}/unit-tests"
                        String publishTestReports = "&& sshpass -p '${globals.reportServerPassword}' rsync ${workspaceFolder}/reports-index.html ${globals.reportServerUser}:${reportServerReportPhysicalRoot}/index.html" 
                            "&& sshpass -p '${globals.reportServerPassword}' rsync ${workspaceFolder}/unit-tests-index.html ${globals.reportServerUser}:${reportServerReportPhysicalRoot}/unit-tests/index.html"
                        String publishCoverageReports = "&& dotnet reportgenerator \"-reports:${workspaceFolder}/coverage.xml\" \"-targetdir:${workspaceFolder}/coverage\" " + 
                           "&& sshpass -p '${globals.reportServerPassword}' rsync -r ${workspaceFolder}/coverage ${globals.reportServerUser}:${reportServerReportPhysicalRoot}"

                        if (testProjectFolder != '') {
                            publishCoverageReports = "&& cd ${workspaceFolder}/${testProjectFolder}" +
                                publishCoverageReports +
                                "&& cd ${workspaceFolder}"
                        }

                        runReports = testDotnet 
                        reportsCommandLine = createReportFolderCommand + publishTestReports 
                            //(testDotnet ? publishTestReports : "") +
                            //(coverageReports ? publishCoverageReports : "")

                        if (testDotnet) {
                            testDotnetCommand += " --logger 'trx;LogFileName=../../results.trx'"
                            reportsMap['Unit Test Report'] = 'unit-tests/index.html'
                        }

                        if (coverageReports) {
                            testDotnetCommand += " /p:CollectCoverage=true  /p:CoverletOutputFormat=opencover"
                            // testDotnetCommand += " /p:CollectCoverage=true "
                            reportsMap['Code Coverage Report'] = 'coverage/index.htm'

                            if (coverageDirectory != '') {
                                testDotnetCommand += " /p:CoverletOutputDirectory='${coverageDirectory}'"
                            }
                        }

                        testDotnetCommand += " /p:NoPackageAnalysis=true"

                        // If runReports is true but no reportFolder was specified, thow an error.
                        if (runReports && !reportFolder) {
                            error("runReports is true but no reportFolder was specified - please specify the reportFolder parameter.")
                            currentBuild.result = 'ABORTED'
                        }
                        reportUrl = "reports/${reportFolder}/${fullVersionNumber}" 
 
                        notificationHtmlFooter = runReports ? "<p>Reports are available at <a href=\'http://${globals.reportServer}/${reportUrl}/\'>View Reports</a></p>" : ""
                        notificationSlackFooter = runReports ? "\nReports are available at <http://${globals.reportServer}/${reportUrl}|View Reports>" : ""
                        // / Reporting variables   

                        // Send build start notification.
                        sendBuildStartNotification notified, jobName, buildNumber, nodeName, buildUrl
                        }
                    catch (Exception e) {
                        // Check for abort due to master build attempt.
                        if (abortedMasterBuild) {
                            currentBuild.result = 'ABORTED'
                            throw e
                            // return here instead of throwing error to keep the build "green"
                            // return
                        }

                        echo e.toString()
                    }
                }
            } // /steps
        } // /stage
        stage('show-job-parameters') {
            steps {               
                echo """
artifactoryNugetUrl: $artifactoryNugetUrl
artifactoryPublishUrl: $artifactoryPublishUrl
artifactoryZipOctopusActionName: $artifactoryZipOctopusActionName
artifactoryZipOctopusProjectId: $artifactoryZipOctopusProjectId
artifactoryZipOctopusStepName: $artifactoryZipOctopusStepName
branchName: $branchName
buildDotnet: $buildDotnet
buildNumber: $buildNumber
buildUrl: $buildUrl
buildUrlHtmlMessage: $buildUrlHtmlMessage
cleanDotnet: $cleanDotnet
cleanWorkspaceAfterRun: $cleanWorkspaceAfterRun
clearNuget: $clearNuget
coverageReports: $coverageReports
dateBuildNumber: $dateBuildNumber
deployNuget: $deployNuget
distNuget: $distNuget
dotnetPackage: $dotnetPackage
fullVersionNumber: $fullVersionNumber
gitChecksum: ${env.GIT_COMMIT}
installDependenciesNuget: $installDependenciesNuget
isChangeRequest: $isPR
isRelease: $isRel
jobName: $jobName
library version: ${globals.version} 
nodeName: $nodeName
notificationHtmlFooter: $notificationHtmlFooter
nupkgFileName: $nupkgFileName
nupkgOctopusActionName: $nupkgOctopusActionName
nupkgOctopusDefaultChannelId: $nupkgOctopusDefaultChannelId
nupkgOctopusProjectId: $nupkgOctopusProjectId
nupkgOctopusStepName: $nupkgOctopusStepName
nupkgOctopusUnstableChannelId: $nupkgOctopusUnstableChannelId
projectName: $projectName
publishFolder: $publishFolder
reportFolder: $reportFolder
reportsCommandLine: $reportsCommandLine
reportServerPhysicalRoot: $reportServerPhysicalRoot
reportUrl: $reportUrl
runReports: $runReports
slackChannel: $slackChannel
testDotnet: $testDotnet
testDotnetCommand: $testDotnetCommand
version: $version
versionFile: $versionFile
versionSuffix: $versionSuffix
workspaceFolder: $workspaceFolder
"""
            }
        }
        // install-dependencies-nuget:	configure artifactory for pull and push of nuget, nuget install
        stage('install-dependencies-nuget') {
            when {
                equals expected: true, actual: installDependenciesNuget
            }
            steps {
                script {                   
                    // Create CI/CD nuget.config file.'
                    echo "Deleting any existing nuget.config files"
                    deleteNugetConfig
                    echo "Writing Nuget config file"
                    writeNugetConfig nugetConfig, "${env.https_proxy}", "${artifactoryNugetUrl}"
                }
            }
        }
        // If buildDotnet set, build the dotnet project
        stage('build-dotnet') {
            when {
                equals expected: true, actual: buildDotnet
            }
            steps {
                // Build the application.
                script {
                    if (cleanDotnet) {
                        echo 'Deleting bin and obj folders from previous builds.'
                        sh "find . -iname 'bin' -o -iname 'obj' | xargs rm -rf"                        
                    }

                    if (clearNuget) {
                        echo "Clearing nuget packages"
                        sh "dotnet nuget locals --clear all"                       
                    }

                    // Update project version numbers.
                    if (versionSuffix != '') {
                        try {
                            updateProjectVersions versionSuffix
                        }
                        catch (Exception e) {
                            echo e.toString()
                        }
                    }
                }

                // Build the project.
                echo "Building project ${projectFile} with config file ${nugetConfig}"
                sh "dotnet build ${projectFile} ${buildDotnetCommand} --configfile ${nugetConfig} --source https://${artifactoryNugetUrl}"
            }
        }
        stage('test-dotnet') {
            when {
                equals expected: true, actual: testDotnet
            }
            steps {
                // Download trxer from Artifactory.
                //sh "curl  --proxy ${https_proxy} -u${globals.artifactoryUserName}:${globals.artifactoryClearTextPassword} -otrxercore.1.0.0.nupkg  https://${artifactoryNugetUrl}/trxercore.1.0.0.nupkg"
                // Unzip trxer.
                //sh 'unzip -j trxercore.1.0.0.nupkg lib/netcoreapp2.0/*'
                // Execute unit tests.
                sh "dotnet test${testDotnetCommand}"
                // Execute trxer.
                //sh "dotnet trxercore.dll results.trx unit-tests-index.html"
            }
        }
        // Copy all test reports to the reports server.
        stage('copy-reports-dotnet') {
            when {
                allOf {
                    equals expected: true, actual: runReports
                    equals expected: false, actual: isPR
                }
            }
            steps {
                // Write the index page for the Reports site folder.
                echo 'Writing reports index file'
                writeReportIndexFile 'reports-index.html', projectName, fullVersionNumber, reportsMap
                // Execute the copy reports commands.
                //sh "${createReportFolderCommand}"

                sh reportsCommandLine
            }
        }
        // Run a special build to populate the dist folder so it can be packaged up
        stage('dist-nuget') {
            when {
                allOf {
                    equals expected: true, actual: distNuget
                    equals expected: false, actual: isPR
                }
            }
            steps {
                // Execute any beforePublishCommand.
                /*
                if (beforePublishCommand != '') {
                    sh "${beforePublishCommand}"
                }
                */

                // Package build outputs into the publish folder.
                sh "dotnet publish --configuration Release -o ${publishPhysicalFolder}  --configfile ${nugetConfig} --source ${artifactoryNugetUrl}"

                // Create the nuspec file and save it in the publish folder.
                script {
                    echo "Writing nuspec file: ${publishPhysicalFolder}/${nuspecFileName}"
                    writeNuspecFile publishPhysicalFolder, nuspecFileName, nuspecId, fullVersionNumber, nuspecAuthor, nuspecDescription

                    if (fileExists("${publishPhysicalFolder}/${nuspecFileName}")) {
                        echo 'Nuspec File written successfully.'
                    } else {
                        echo 'ERROR writing Nuspec file!'
                    }
                }
            }
        }
        // Package up output in dist folder as a nuget 
        stage('package-nuget') {
            when {
                allOf {
                    equals expected: true, actual: distNuget
                    equals expected: false, actual: isPR
                }
            }
            steps {
                // Move to the publishPhysicalFolder folder and create the nupkg file in the workspace folder using zip.
                echo "Package up output in dist folder as a nuget"
                sh "cd ${publishFolder} && zip -r ${workspaceFolder}/${nupkgFileName} . && cd ${workspaceFolder}"
            }
        }
        // Push nuget build to artifactory, make new octopus build pointing to that
        stage('deploy-nuget') {
            when {
                allOf {
                    equals expected: true, actual: deployNuget
                    equals expected: false, actual: isPR
                }
            }
            steps {
                echo "Publish nuget build to Artifactory"
                sh "dotnet nuget push ${nupkgFileName} --api-key ${globals.artifactoryUserName}:${globals.artifactoryNugetPassword} --source ${artifactoryPublishUrl}"

                echo "Make new octopus build pointing to that"
                // Create the JSON file and save it in the current folder.
                createOctopusRelease(
                    projectId: nupkgOctopusProjectId, 
                    version: fullVersionNumber, 
                    stepName: nupkgOctopusStepName, 
                    actionName: nupkgOctopusActionName, 
                    octopusChannelId: octopusChannelId,
                    buildUrl: buildUrl
                )
            }
        }
        stage('cleanup') {
            when {
                equals expected: true, actual: cleanWorkspaceAfterRun
            }
            steps {
                cleanWs()
            }
        }
    }
    post {
            // Send notifications after the job completes.
            success {
                emailext (
                    recipientProviders: [[$class: 'DevelopersRecipientProvider']],
                    to: "${notified}",
                    mimeType: 'text/html',
                    subject: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]",
                    body: "<p>SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                
            }
            failure {
                emailext (
                    recipientProviders: [[$class: 'CulpritsRecipientProvider']],
                    to: "${notified}",
                    mimeType: 'text/html',
                    subject: "ERROR: Job ${jobName} [${fullVersionNumber}]",
                    body: "<p>ERROR: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                
            }            
        }
    }
}
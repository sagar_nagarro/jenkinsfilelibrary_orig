import groovy.transform.TypeChecked
import globals
 
@TypeChecked
def call(Map parms) {

    // nodeLabel controls which Jenkins slaves this project can build on, and is required.
    String nodeLabel = required value: parms.get('nodeLabel'), message: "'nodeLabel' parameter not set in the Jenkinsfile.  This parameter is required."

    pipeline {
        agent {
            node {
                label nodeLabel
                customWorkspace URLDecoder.decode("/home/jenkins/workspace/${JOB_NAME}", "UTF-8")
            }    
        }
        stages {
            // init Phase 2 (always executes): - Setup variables and print out values for debugging
            stage('init') {
                steps {
                    script {
                        echo 'Executed step'
                    }
                }
            }
        }
    }
}
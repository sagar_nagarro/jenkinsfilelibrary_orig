// Function that works around anomalies with Groovy | Jenkins | Java reading files.
def call(String fileName) {
    if (!fileName) {
        error('FATAL: readFileEx was called with a null file name!')
    }
    else {
        String fileContents = readFile file: fileName, encoding: 'UTF8'
        // Trim leading question marks that seem to appear possibly if files were saved as UTF8(?)
        String updatedContents = fileContents.replaceFirst(/^(\s*\?+)/, '')
        
        return updatedContents
    }
}

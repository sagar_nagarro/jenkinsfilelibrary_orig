// Function that tests for the existence of a parameter
// and throws an error if it is not set.
def call(Map parms) {
    def value = parms.get("value")

    if (!value) {
        error(parms.get("message"))
    }

    return value
}

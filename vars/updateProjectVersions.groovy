// Function that updates version numbers in project files.
def call(String versionSuffix, Boolean subFolders = true, String rootFolder = '') {
    echo "updating project versions: versionSuffix: ${versionSuffix}, subFolders: ${subFolders}, rootFolder: ${rootFolder}"

    def readPackageVersion = { packageFileName -> 
        try {
            String packageFileContents = readFile packageFileName
            def rxVersion = packageFileContents =~ /<PackageVersion>(.*)<\/PackageVersion>/
            return rxVersion ? rxVersion[0][1] : ''
        }
        catch (Exception e) {
            println e.toString()
        }
    }

    def writePackageVersion = { packageFileName, packageVersion ->
        try {
            // Write the package version number.
            echo "writePackageVersion: Updating package version of file ${packageFileName} to ${packageVersion}${versionSuffix}"
            String packageFileContents = readFile packageFileName
            String updatedContents = packageFileContents.replaceFirst(/(<PackageVersion>)([^<]+)/, '$1$2' + "${versionSuffix}")
            //println updatedContents
            writeFile file: packageFileName, text: updatedContents, encoding: "UTF8"
            }
        catch (Exception e) {
            println e.toString()
            }
    }

    def writeVersion = { versionFileName, version ->
        try {
            String versionFileContents
            String updatedContents

            // Write the package version number.
            if (version == '') {
                version = '1.0.0'
                echo "writeVersion: Inserting version ${version}${versionSuffix} into file ${versionFileName}"
                versionFileContents = readFile versionFileName
                updatedContents = versionFileContents.replaceFirst(/(<\/PackageId>)/, '$1' + "${version}${versionSuffix}")
            }
            else {
                echo "writeVersion: Updating version of file ${versionFileName} to ${version}${versionSuffix}"
                versionFileContents = readFile versionFileName
                updatedContents = versionFileContents.replaceFirst(/(<Version>)([^<]+)/, '$1$2' + "${versionSuffix}")
            }

            //println updatedContents
            writeFile file: versionFileName, text: updatedContents, encoding: "UTF8"
            }
        catch (Exception e) {
            println e.toString()
            }
    }

    def writePackageJsonVersion = { versionFileName, version ->
        try {
            // Write the package version number.
            echo "writePackageJsonVersion: Updating version of file ${versionFileName} to ${version}${versionSuffix}"
            String versionFileContents = readFile versionFileName
            String updatedContents = versionFileContents.replaceFirst(/(\"version\": \")(\d+\.\d+\.\d+)(\")/, '$1' + version + versionSuffix + '$3')
            echo updatedContents
            writeFile file: versionFileName, text: updatedContents, encoding: "UTF8"
            }
        catch (Exception e) {
            println e.toString()
            }
    }

    String folderSpec = subFolders ? '**/' : ''

    def csprojFiles = findFiles glob: (rootFolder != '' ? rootFolder + '/' : '') + folderSpec + '*.csproj'
    echo "${csprojFiles.length} C# projects found"

    csprojFiles.each { csprojFile ->
        private String projectFileName = csprojFile.getPath()

        sh "iconv -c -f utf8 -t ascii $projectFileName -o $projectFileName"
        private String packageVersion = readPackageVersion projectFileName
        private String version = readVersion projectFileName

        // If there is a package version, use it.
        if (packageVersion != '') {
            writePackageVersion projectFileName, packageVersion
            echo "${projectFileName} - Package Version ${packageVersion}"
        }
        // Otherwise, use the version.
        else if (version != '') {
            writeVersion projectFileName, version
            echo "${projectFileName} - Version ${version}"
        }
        // If neither, assume version 1.0.0. and insert into the file.
        else {
            echo "WARNING: No Version Number found in project file: ${projectFileName} - assuming '1.0.0'"
            writeVersion projectFileName, ''
        }
    }
            
    // Check for sub-projects and if found, update those package.jsons.
    def subPackageJsons = findFiles glob: (rootFolder != '' ? rootFolder + '/' : '') + folderSpec + 'package.json'
    echo "${subPackageJsons.length} sub-packages found"

    subPackageJsons.each { subPackageJson ->
        private String packageFileName = subPackageJson.getPath()

        sh "iconv -c -f utf8 -t ascii $packageFileName -o $packageFileName"
        private String version = readVersion packageFileName

        writePackageJsonVersion packageFileName, version
    }

}
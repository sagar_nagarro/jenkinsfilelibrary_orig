import groovy.transform.TypeChecked
import globals

@TypeChecked
def call(Map parms) {
    // Project Definition Parameters
    String nodeLabel = required value: parms.get('nodeLabel'), message: "'nodeLabel' parameter not set in the Jenkinsfile.  This parameter is required."
    String versionFile = required value: parms.get('versionFile'), message: "'versionFile' parameter not set in the Jenkinsfile.  This parameter is required."

    // Process control
    Boolean installDependencies = parms.get("installDependencies", false)
    String installDependenciesCommand = parms.get("installDependenciesCommand", "")

    Boolean npmBuild = parms.get("npmBuild", false)
    String buildCommand = parms.get("buildCommand", "")
    String publishFolder = parms.get("publishFolder", "")
    String publishFolderPathSegment = publishFolder ? publishFolder + '/' : ''
    Boolean updateVersions = parms.get("updateVersions", false)

    Boolean npmLint = parms.get("npmLint", false)
    String lintCommand = parms.get("lintCommand", "")
    Boolean npmTest = parms.get("npmTest", false)
    String testCommand = parms.get("testCommand", "")
    Boolean npmCoverage = parms.get("npmCoverage", false)
    String coverageCommand = parms.get("coverageCommand", "")
    Boolean runNpmTests = npmLint || npmTest || npmCoverage
    String testCommandLine = "echo " +
        (npmLint ? "&& npm run lint${lintCommand}" : "") +
        (npmTest ? "&& npm run test${testCommand}" : "") +
        (npmCoverage ? "&& npm run coverage${coverageCommand}" : "")
    
    Boolean testReports = parms.get("testReports", false)
    Boolean coverageReports = parms.get("coverageReports", false)

    Boolean npmPackage = parms.get("npmPackage", false)
    Boolean nugetPackage = parms.get("nugetPackage", false)
    String packageCommand = parms.get("packageCommand", "")

    Boolean npmPublish = parms.get("npmPublish", false)
    Boolean nugetPublish = parms.get("nugetPublish", false)
    String publishCommand = parms.get("publishCommand", "")

    // notify	(always executes): send emails to "notified" folks and send slack msg to slack channel
    String notified = parms.get("notified", "")

    // Define pipeline global variables.
    // TODO: Throw errors if required parms not set.
    String npmRepoName = parms.npmRepoName
    String npmScope = parms.npmScope
    Boolean npmClean = parms.get("npmClean", false)
    Boolean npmCacheVerify = parms.get("npmCacheVerify", false)

    String reportFolder = parms.reportFolder
    String reportServerPhysicalRoot = "/var/www/reports/${reportFolder}"
    String reportServerPhysicalFolder
    Boolean runReports
    String reportsCommandLine

    String branchName
    String buildNumber

    String projectName = parms.get("projectName")
    String version
    String versionSuffixBase
    String nodeName
    String workspaceFolder
    String jobName
    String buildUrl
    String buildUrlSlackMessage
    String buildUrlHtmlMessage
    String octopusChannelId

    String dateBuildNumber
    String fullVersionNumber
    String reportUrl
    String notificationHtmlFooter = ''
    String notificationSlackFooter = ''

    String npmArtifactoryUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/npm/${npmRepoName}"
    String artifactoryPublishUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${parms.get('artifactoryPublishRepo', '')}/"
    
    String nupkgOctopusProjectId = parms.get("nupkgOctopusProjectId", "")
    String nupkgOctopusStepName = parms.get("nupkgOctopusStepName", "")
    String nupkgOctopusActionName = parms.get("nupkgOctopusActionName", "")
    String nupkgOctopusUnstableChannelId = parms.get("nupkgOctopusUnstableChannelId", "")
    String nupkgOctopusDefaultChannelId = parms.get("nupkgOctopusDefaultChannelId", "Default")

    Boolean nugetBuild = parms.get("nugetBuild", false)
    String nugetBaseFileName = parms.get("nugetBaseFileName", "")
    String nuspecBaseFileName = parms.get("nuspecBaseFileName", "")
    String nuspecFolder = parms.get("nuspecFolder", ".")
    String nuspecAuthor = parms.get("nuspecAuthor", "")
    String nuspecDescription = parms.get("nuspecDescription", "Placeholder for Nuspec Description")
    String nupkgFileName
    String nuspecFileName
    Boolean nugetDeploy = parms.get("nugetDeploy", false)

    String slackChannel = parms.slackChannel
    Boolean isPR = isPullRequest()
    Boolean isRel = isRelease()
    
pipeline {
    agent {
        node {
            label nodeLabel
            customWorkspace URLDecoder.decode("/home/jenkins/workspace/${JOB_NAME}", "UTF-8")
        }    
    }
    options {
        timeout(time: 1, unit: 'HOURS')
    }
    stages {
        // Initialize pipeline.
        // Environment has been set up by now, so it is safe to set variables from it.
        stage('init') {
            steps {
                script {
                    Boolean abortedMasterBuild = false

                    try {
                        // Set proxies for dotnet.
                        env.http_proxy = "http://bcpxy.nycnet:8080"
                        env.https_proxy = "http://bcpxy.nycnet:8080"

                        // Get environment values set by Jenkins.
                        branchName = "${env.BRANCH_NAME}"
                        buildNumber = "${env.BUILD_NUMBER}"
                        jobName = "${env.JOB_NAME}"
                        nodeName = "${env.NODE_NAME}"
                        workspaceFolder = "${env.WORKSPACE}"
                        buildUrl = "${env.RUN_DISPLAY_URL}"
                        buildUrlSlackMessage = "Click <${buildUrl}|here> to view job details"
                        buildUrlHtmlMessage = "<p>Click <a href=\'${buildUrl}\'>here</a> to view job details.</p>"

                        // Prevent builds against master.
                        if (branchName == 'master') {
                            abortedMasterBuild = true
                            echo "Attempt to build from MASTER branch - aborting..."
                            error("Attempt to build from MASTER branch - aborting...")
                        }

                        if (npmClean) {
                            sh "rm -rf node_modules"
                            sh "npm cache clean --force"
                        }

                        if (npmCacheVerify) {
                            sh "npm cache verify"
                        }

                        // Read the version from the version file.
                        version = readVersion versionFile
                       
                        // Generate unique date / build stamp for file names
                        dateBuildNumber = buildSerialNumber buildNumber

                        // Generate the full version number.
                        fullVersionNumber = generateVersionNumber version, branchName, dateBuildNumber

                        // Extract the version suffix.
                        versionSuffix = fullVersionNumber.substring(version.length());

                        // get the channelID
                        octopusChannelId = branchName =~ /^(feature\/|develop)/ ? "${nupkgOctopusUnstableChannelId}": "${nupkgOctopusDefaultChannelId}"

                        reportUrl = "reports/${reportFolder}/${dateBuildNumber}"    

                        String createFolderCommand = "sshpass -p '${globals.reportServerPassword}' ssh -p 22 ${globals.reportServerUser} mkdir -p ${reportServerPhysicalRoot}"
                        String publishTestReports = "&& sshpass -p '${globals.reportServerPassword}' rsync -r reports/ ${globals.reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"
                        String publishCoverageReports = "&& sshpass -p '${globals.reportServerPassword}' rsync -r coverage ${globals.reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"

                        runReports = testReports || coverageReports
                        reportsCommandLine = createFolderCommand +
                            (testReports ? publishTestReports : "") +
                            (coverageReports ? publishCoverageReports : "")

                        // If runReports is true but no reportFolder was specified, thow an error.
                        if (runReports && !reportFolder) {
                            error("runReports is true but no reportFolder was specified - please specify the reportFolder parameter.")
                            currentBuild.result = 'ABORTED'
                        }

                        notificationHtmlFooter = runReports? "<p>Reports are available at <a href=\'http://${globals.reportServer}/${reportUrl}/\'>View Reports</a></p>" : ""
                        notificationSlackFooter = runReports ? "\nReports are available at <http://${globals.reportServer}/${reportUrl}|View Reports>" : ""

                        nupkgFileName = "${nugetBaseFileName}.${fullVersionNumber}.nupkg"
                        nuspecFileName = "${nuspecBaseFileName}.${fullVersionNumber}.nuspec"

                        // If the project uses npm, configure it for the job.
                        if (installDependencies || npmBuild || runNpmTests || npmPackage || npmPublish) {
                            echo "Configure npm to use Artifactory"
                            sh "npm config set ${npmScope}:registry https://${npmArtifactoryUrl}/"

                            sh "npm config set //${npmArtifactoryUrl}:_password ${globals.artifactoryNpmPassword}"
                            sh "npm config set //${npmArtifactoryUrl}:username ${globals.artifactoryUserName}"
                            sh "npm config set //${npmArtifactoryUrl}:email ${globals.artifactoryEmail}"
                            sh "npm config set //${npmArtifactoryUrl}:always-auth true"
                        }

                        // Send build start notification.
                        sendBuildStartNotification notified, jobName, buildNumber, nodeName, buildUrl
                    }
                    catch (Exception e) {
                        // Check for abort due to master build attempt.
                        if (abortedMasterBuild) {
                            currentBuild.result = 'ABORTED'
                            throw e
                            // return here instead of throwing error to keep the build "green"
                            // return
                        }

                        echo e.toString()
                    }
                }

            } // /steps
        } // /stage
        stage('show-job-parameters') {
            steps {               
            echo """
artifactoryPublishUrl: $artifactoryPublishUrl
branchName: $branchName
buildCommand: $buildCommand
buildNumber: $buildNumber
buildUrl: $buildUrl
coverageCommand: $coverageCommand
coverageReports: $coverageReports
dateBuildNumber: $dateBuildNumber
fullVersionNumber: $fullVersionNumber
gitChecksum: ${env.GIT_COMMIT}
installDependencies: $installDependencies
installDependenciesCommand: $installDependenciesCommand
isChangeRequest: $isPR
jobName: $jobName
lintCommand: $lintCommand
nodeName: $nodeName
npmArtifactoryUrl: $npmArtifactoryUrl
npmBuild: $npmBuild
npmCacheVerify: $npmCacheVerify
npmClean: $npmClean
npmCoverage: $npmCoverage
npmLint: $npmLint
npmPackage: $npmPackage
npmPublish: $npmPublish
npmRepoName: $npmRepoName
npmScope: $npmScope
npmTest: $npmTest
nugetBaseFileName: $nugetBaseFileName
nugetBuild: $nugetBuild
nugetDeploy: $nugetDeploy
nugetPackage: $nugetPackage
nugetPublish: $nugetPublish
nupkgFileName: $nupkgFileName
nupkgOctopusActionName: $nupkgOctopusActionName
nupkgOctopusProjectId: $nupkgOctopusProjectId
nupkgOctopusStepName: $nupkgOctopusStepName
nupkgOctopusUnstableChannelId: $nupkgOctopusUnstableChannelId
nupkgOctopusDefaultChannelId: $nupkgOctopusDefaultChannelId
nuspecAuthor: $nuspecAuthor
nuspecBaseFileName: $nuspecBaseFileName
nuspecDescription: $nuspecDescription
nuspecFileName: $nuspecFileName
nuspecFolder: $nuspecFolder
octopusApiKey: $globals.octopusApiKey
octopusUrl: $globals.octopusUrl
packageCommand: $packageCommand
projectName: $projectName
publishCommand: $publishCommand
publishFolder: $publishFolder
reportFolder: $reportFolder
reportsCommandLine: $reportsCommandLine
reportServer: $globals.reportServer
reportServerPassword: $globals.reportServerPassword
reportServerPhysicalFolder: $reportServerPhysicalFolder
reportServerPhysicalRoot: $reportServerPhysicalRoot
reportUrl: $reportUrl
runNpmTests: $runNpmTests
runReports: $runReports
slackChannel: $slackChannel
testCommand: $testCommand
testCommandLine: $testCommandLine
testReports: $testReports
version: $version
versionFile: $versionFile
versionSuffixBase: $versionSuffixBase
workspaceFolder: $workspaceFolder
"""
            }
        }
        // If installDependencies set, install dependencies from the Artifactory NPM registry.
        stage('install-dependencies') {
            when {
                equals expected: true, actual: installDependencies
            }
            steps {
                // Configure npm to use Artifactory for both retrieving npms *from* Artifactory (all app front-ends)
                // and also for pushing npms *to* Artifactory (CAF-Angular only)
                // TODO: Encapsulate duplicate copies of this code into a method.
                // First confirm that npm config settings persist for lifecycle of the Jenkins job
                sh "npm install ${installDependenciesCommand}"
                echo "npm install completed"
            }
        }
        // If npmBuild set, execute 'npm run' 
        stage('build-npm') {
            when {
                equals expected: true, actual: npmBuild
            }
            steps {
                // Build the application.
                sh "npm run ${buildCommand}"
            }
        }
        // If nugetBuild set, write a nuspec file
        stage('build-nuget') {
            when {
                equals expected: true, actual: nugetBuild
            }
            steps {
                script {
                    echo "Writing nuspec file: ${nuspecFolder}/${publishFolderPathSegment}${nuspecFileName}"
                    writeNuspecFile nuspecFolder, publishFolderPathSegment + nuspecFileName, nuspecBaseFileName, fullVersionNumber, nuspecAuthor, nuspecDescription
                }
            }
        }
        // If updateVersions set, update the versions in all target files found in the project.
        stage('update-versions') {
            when {
                equals expected: true, actual: updateVersions
            }
            steps {
                echo "Versioning ${versionFile}"
                script {
                    updateProjectVersions "${buildNumber}${versionSuffixBase}"
                }
            }
        }
        // Run tests on the project.
        stage('npm-test') {
            when {
                equals expected: true, actual: runNpmTests
            }
            steps {
                sh "${testCommandLine}"
            }
        }
        // Copy test reports to the reports server.
        stage('copy-reports') {
            when {
                allOf {
                    equals expected: true, actual: runReports
                    equals expected: false, actual: isPR
                }
            }
            steps {
                sh reportsCommandLine
            }
        }
        // Should only run for applications
        stage('npm-package') {
            when {
                allOf {
                    equals expected: true, actual: npmPackage
                    equals expected: false, actual: isPR
                }
            }
            steps {
                sh "npm run package${packageCommand}"
            }
        }
        stage('nuget-package') {
            when {
                allOf {
                    equals expected: true, actual: nugetPackage
                    equals expected: false, actual: isPR
                }
            }
            steps {
                // Move to the nuspecFolder folder and create the nupkg file in the parent folder using zip.
                sh "cd ${nuspecFolder} && zip -r ../${nupkgFileName} ."
            }
        }
        stage('npm-publish') {
            when {
                allOf {
                    equals expected: true, actual: npmPublish
                    equals expected: false, actual: isPR
                }
            }
            steps {
                echo "Publishing npms to Artifactory"
                sh 'chmod -R 755 ./scripts'
                sh "npm publish --registry https://${npmArtifactoryUrl} ${publishCommand}"
            }
        }
        stage('nuget-publish') {
            when {
                allOf {
                    equals expected: true, actual: nugetPublish
                    equals expected: false, actual: isPR
                }
            }
            steps {
                echo "Publish zip to Artifactory"
                sh "dotnet nuget push ${nupkgFileName} --api-key ${globals.artifactoryUserName}:${globals.artifactoryNugetPassword} --source https://${artifactoryPublishUrl}"
            }
        }
        stage('nuget-deploy') {
            when {
                allOf {
                    equals expected: true, actual: nugetDeploy
                    equals expected: false, actual: isPR
                }
            }
            steps {           
                echo "Creating new Octopus Release which will trigger auto-deploy"
                // Create the JSON file and save it in the current folder.
                createOctopusRelease(
                    projectId: nupkgOctopusProjectId, 
                    version: fullVersionNumber, 
                    stepName: nupkgOctopusStepName, 
                    actionName: nupkgOctopusActionName, 
                    octopusChannelId: octopusChannelId,
                    buildUrl: buildUrl
                )
           }
        }
    }
    post {
            // Send notifications after the job completes.
            success {
                emailext (
                recipientProviders: [[$class: 'DevelopersRecipientProvider']],
                to: "${notified}",
                mimeType: 'text/html',
                subject: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                slackSend channel: slackChannel, color: "#00FF00", message: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}${notificationSlackFooter}"
            }
            failure {
                emailext (
                recipientProviders: [[$class: 'CulpritsRecipientProvider']],
                to: "${notified}",
                mimeType: 'text/html',
                subject: "ERROR: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>ERROR: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                slackSend channel: slackChannel, color: "#FF0000", message: "ERROR: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}${notificationSlackFooter}"
            }
        }
    }
}
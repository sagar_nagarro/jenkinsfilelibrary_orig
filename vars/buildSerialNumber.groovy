// Function that creates a unique serial number for a build 
// based on the current date and the Jenkins Build Number.
def call(String buildNumber) {
    // Generate unique date / build stamp for file names
    Calendar cal = new GregorianCalendar()

    String dateBuildNumber = 
        String.format('%1$tY%1$tm%1$td%1$tH%1$tM%1$tS', cal) +
        ".${buildNumber}"

    return dateBuildNumber    
}

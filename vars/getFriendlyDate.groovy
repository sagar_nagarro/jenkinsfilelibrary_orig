// Function that creates a friendly date 
// based on the current date.
def call() {
    // Generate unique date / build stamp for file names
    Calendar cal = new GregorianCalendar()

    String friendlyDate = 
        String.format('%1$tY-%1$tm-%1$td-%1$tH-%1$tM-%1$tS', cal)

    return friendlyDate    
}

class npm {

    private cafConfig _config

    public npm(cafConfig config) {
        _config = config
    }

    void install(String commands) {
        commands = commands ? commands : ""
        sh "npm install $commands"
    }

    void run(String commands) {
        commands = commands ? commands : ""
        sh "npm run $commands"
    }

    void publish() {
        commands = commands ? commands : ""
        sh "npm install $commands"
    }

}

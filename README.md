# Common Application Framework (CAF) Jenkinsfile Library

This repository contains common variable definitions and functions for Jenkins buildfiles used for CAF-compliant projects.
A Jenkinsfile uses declarative language and Groovy functions to define all of the build steps for Continuous Integration, Continuous Test, and Continuous Deployments.

# Table of Contents
 1. CI/CD/CT Components
 2. CAF-Compliant Application Structure
 3. Quickstart
      1. Prepare your project for CI/CD/CT
      2. Setup Jenkins build
      3. Setup Octopus Deployment
 4. CAF Artifact Versioning Strategy
      1. Concept of Operations
      2. GitFlow Recommendations
      3. Current Limitations
      4. Versioning of Dependencies
 5. Jenkinsfile Shared Library 'caf-utils'

# CI/CD/CT Components

* Builds are done via Jenkins Blue Ocean, which is installed on-prem
* All third party dependencies including npm and nuget files are contained in a centralized cloud-based Artifactory repo
* All output artfacts built by Jenkins are uploaded into the cloud-based Artifactory repo
* Automated test reports are uploaded into a website.
* Octopus Deploy performs automated deployments into a "CD" environment.  All other environments are set for manual trigger.
* We use the `nuget` format to package up everything for deployment-- for *both* front-end and back-end projects.  `nuget` is nothing more than a fancy zip file.

# CAF-Compliant Application Structure

* CAF-compliant applications get the all benefits of the CI/CD/CT pipeline by following a few simple conventions.
* CAF-compliant applications will have (at least) two BitBucket repositories: one for the Angular front-end and one for the C#.NET Core backend.
* Each repo has its own Jenkinsfile, located at the very top level
* The central Jenkinsfile library takes care of versioning
* For commits to feature branches or pull requests, Jenkins performs a build and test.
* For merges to `develop` or above, Jenkins builds, tests, reports, packages, uploads the nuget payload to Artifactory, and then creates a new release in Octopus.
* The Octopus release has exactly the same name as the new artifact in Artifactory, and the Octopus release package points to the Artifactory nuget artifact.  For all intents and purposes they can be considered the same thing. 
* Octopus is configured to automatically deploy any new release that is created to the CD environment.  You can see this by looking at the development environment in Octopus and seeing the lightning bolt next to it.  Other environments like UAT, QA, Staging, Training, and Prod will not have a lightning bolt.

# Quickstart

## Step 1. Prepare your project for CI/CD/CT
If you do some prep work up-front, establishing CI/CD/CT can be done in a few hours or less.  

### 1.1: Download and test GitFlow plugin
* The entire CI/CD/CT pipeline is based on GitFlow which prescribes a strict process that takes time to get comfortable with.
* Create a `develop` branch and use feature branches off of it for all of your development.  Every developer must initialize GitFlow, accepting all the defaults.
* Take some time and practice creating "feature" branches for your defects and user stories, practice pull requests and merging to develop via "feature start" and "feature finish".
* Practice creating releases and merging to master via "release start" and "release finish" commands
* Checkout the videos on Youtube to learn GitFlow quickly https://www.youtube.com/results?search_query=gitflow
* You should check the BitBucket configuration `Require issue keys in commit messages` in Settings --> Links and ensure it is turned __ON__ for each of your repositories
* See the versioning discussion at the bottom of this file for more details

### 1.2: Understand SemVer 2.0 versioning rules and add version metadata
* The CI/CD/CT process creates outputs that are versioned strictly according to the SemVer 2.0 standard.
* No source code is ever changed by the build.  The versions of the output nukpg files are created on the fly.
* The output of a repo will be a `nuget` file, such as `AIMS.Server.nupkg` that gets uploaded to Artifactory
* The build system uses the version from `CSPROJ` or `package.json` and appends branch and build information (see step 1.4 below)
* See https://semver.org for more information on semantic versioning
* The format of the resulting artifact in Artifactory (and release in Octopus) is the following:
```
   [Project Name].[Version number].[Build date/time].[Build number].nupkg
```
* Examples:
```
   AIMS.Server.1.0.0-unstable.20180524113121.2.nupkg   (build #2, develop branch)
   AIMS.Client.0.4.2.20180424100932.133.nupkg          (build 133, master branch)
   CapDash.Client.3.1.0-hotfix.20180424100932.13.nupkg (build 13, hotfix branch)
```

### 1.3: Document all of your existing environments
* Document each environment, for example: development, integration, QA, UAT, Training, Staging, Production
* For each environment, provide the following information for each server: CPU, Disk, OS, Version, IP address, DNS name, root login, root password
* Capture a picture to describe the topography of the environment, including front-end server, back-end server, and database
* You can ask the build team for an example that you can update with your own information.
* This information will be used to configure Octopus Deploy

### 1.4: Create a valid version number for each project
* Each front-end project `package.json` file should contain a valid version number, version `0.0.1` at a minimum.  Do not leave the version as `0.0.0`
* For example:
```
  "name": "aims-client",
  "version": "1.0.0",
```
* Each back-end project `xxx.CSPROJ` file should contain a valid version number, version `0.0.1` at a minimum.  For example: 
```
  <PropertyGroup>  
    <IsPackable>true</IsPackable>
    <GeneratePackageOnBuild>true</GeneratePackageOnBuild>
    <Version>1.0.0</Version>
    <Summary>Application Inventory Management System</Summary>
    <Description>Application Inventory Management System provides a cdentralized place to track applications, infrastructure, projects and resource management</Description>
    <Tags>Porfolio Analysis, Project Management, DSS-Wide</Tags>
  </PropertyGroup>
```

### 1.5: Ensure you can build locally for each project with no (or minimal) build warnings
* Ideally every project would build 100% clean with no warnings 
* Spurious warnings should be supressed using pragmas

### 1.6: Ensure both client and server projects include linting
* Client side should be linted with `tslint`
* Server side should be linted via `FxCop` 
* Make sure no warnings or errors are reported
* Note: it may take multiple days to resolve all warnings and errors, depending on the size of your repository and whether you have ever linted the code before.
* Check out the examples in AIMS.client and AIMS.server

### 1.7: Ensure both client and server projects include unit tests
* Client side should be tested with `karma` and `jasmine`
* Client side results reporters: (inline text) `karma-spec-reporter` and (html) `karma-junit-reporter`
* Client side coverage reporter: `karma-coverage-istanbul-reporter`
* Server side should be tested via `xUnit` 
* Server side results HTML via configurations in the `CSPROJ` file
* Server side coverage with `coverlet`
* Make sure no warnings or errors are reported
* Ensure that both a test results report and code coverage report are generated for each
* Check out the examples in AIMS.client and AIMS.server

### 1.8: Ensure both client and server projects include BDD tests
* Client side should be tested via `CucumberJS` and `Protractor`
* Client side reporter `cucumber-html-reporter`
* Server side should be tested via `SpecFlow`
* Server side reporting via `SpecFlow+`
* Make sure no warnings or errors are reported
* Ensure that both a test results report and code coverage report are generated for each
* Check out the examples in AIMS.client and AIMS.server

## Step 2. Setup Jenkins build
Setup a Jenkins build for all project repos.  We may need additional build resources.

### 2.1: Put Jenkinsfile into each repo
Copy a Jenkinsfile from a similar project into the top-level folder and modify to suit.  See examples: AIMS.Client or AIMS.Server

### 2.2: Create a Jenkins Blue Ocean project for each repo
There is a wizard in Jenkins Blue Ocean that searches for a top-level Jenkinsfile in a repo and automatically creates a job.

### 2.3: Create additional Jenkins build slave if needed
Consult with the build team to see if addition build resources are needed.  Your project may need a dedicated Jenkins build slave.

## Step 3. Setup Octopus Deployment
Make sure an Octopus Deploy project is setup for all project repos.  Create environments automatically using Terraform and Chef as needed.
A given application may have any number of environments such as `dev-integration`, `qa`, `performance-test`, `training`, `demo`, `staging`, and `prod`.
At a minimum, you should have `dev-integration` (abbreviated as `dev`), `qa`, `staging`, and `prod`.  

### 3.1: Create an Octopus Project for each repo
* Copy an Octopus project from a similar project and modify to suit.  For example: AIMS.Client or AIMS.Server
* Modify the deployment steps to suit

### 2.2: Create Environments, Lifecycles, and Channels for all infrastructure
* Create an Octopus environment for each physical environemnt
* Create two lifecycles: one for regular, one for unstable
* Create a non-default channel "unstable" for unstable builds
* Please refer to the AIMS configuration as needed.

### 2.3: Ensure test users are set up as needed
* Create an Octopus team for anyone who needs to be able to manually deploy your application to QA, Training, Demo, UAT, Staging, or Prod
* You can create multiple teams with specific access to specific environments as needed
* Please refer to the AIMS configuration as needed.

-----

# CAF Artifact Versioning Strategy

## Concept of Operations

*	The CI/CD/CT Pipeline builds projects and uploads artifacts into Artifactory, while applying a guaranteed unique version number to every build.
*	Version numbers consist of both a _human-maintained_ and a _machine-maintained_ component.   The human-maintained version complies with semver and the machine-maintained version is appended (e.g. `AIMS.Server.1.0.0-unstable.20180524105431.3.nupkg`).   Human maintained piece is `1.0.0` machine maintained is `-unstable.20180524105431.3`
*	The human-maintained version number complies with semver (e.g. major.minor.patch), and it identifies real functional changes.  
*	The machine-maintained version components identify things like:
      * The branch that was used (e.g. `-unstable` for develop branch, `-hotfix` for hotfix branch)
      * The date/time on which the build was generated (e.g. `20180530112302`)
      * The Jenkins build number (e.g. `22`)
      * The git changeset checksum (NOTE: we are not adding this today, but could easily do so)
* For any given release, a human decides whether the updates comprise a major, minor, or patch change.   
* The human must update the version number in `package.json` or `CSPROJ`—as of today, the build does not make any changes to human-maintained version numbers. 

## GitFlow Recommendations

* Name your `develop` branch __exactly__ as `develop` please do not use `Development` or any other name.  It causes confusion and the build scripts may break.
* In the GitFlow implementations we have seen, release branches are made frequently—much more frequently than for other branching models.
* GitFlow releases and hotfixes are the only place where version numbers are incremented.   A pull request _never_ increments a version number.   Note the implication of this is that the `develop` branch only gets an updated version number when a release branch is merged down to it via `gitflow release finish`
* A GitFlow release may increment a major version, minor version, or even a patch version.
* Multiple release branches can be active simultaneously, so you could theoretically have a `1.2.3`, a `1.3` and a `2.0` release all active at the same time (not recommended).
* See: https://danielkummer.github.io/git-flow-cheatsheet/
* NOTE: nothing in the CI/CD/CT framework forces a project to work according to the GitFlow recommendations.  Projects are free to increment version numbers in their pull requests if they choose to do so.  

## Current Limitations

* What if we wanted the build to automatically increment the patch number?
* The problem is that there is no way for the build process to easily know what a developer “meant” to do.   
* What if there is more than one `package.json` file?  Should they all be updated?  Some?  Only one?  How can we automatically tell which package.json goes with which piece of code?

## Versioning of Dependencies

There is no reason to bump up the version of a dependent package just because your package’s patch version changed.  For example, if `CAF.Bootstrapper` moves from `1.0.18` to `1.0.19`, and `CAF.Logging.Http` depends on `CAF.Bootstrapper`, there is no reason to bump the version number of the logging package.   `CAF.Logging.Http` version `1.0.1` can be compatible with both `1.0.18` and `1.0.19` of `CAF.Bootstrapper` in accordance with the semver rules.    Of course, we would not expect logging to be compatible with a new major version of bootstrapper. 

# Jenkinsfile Shared Library 'caf-utils'

We have defined a shared library where many templates and reusable groovy functions are implemented.  These are made available to your Jenkinsfile via the following line:


```
import globals
@Library('caf-utils') _
```

caf-utils is the _name_ of the shared library repository.  It is located here: https://bitbucket.org/dhsit/jenkinsfilelibrary 
The directory structure of a Shared Library repository is as follows:

```
(root)
+- src                     # Groovy source files
|   +- org
|       +- foo
|           +- Bar.groovy  # for org.foo.Bar class
+- vars
|   +- foo.groovy          # for global 'foo' variable
|   +- foo.txt             # help for 'foo' variable
+- resources               # resource files (external libraries only)
|   +- org
|       +- foo
|           +- bar.json    # static helper data for org.foo.Bar
```

The src directory looks like standard Java source directory structure. This directory is added to the classpath when executing Pipelines.
The vars directory hosts scripts that define global variables accessible from Pipeline. 
The basename of each `*.groovy` file is a Groovy (~ Java) identifier, conventionally camelCased. 
Each `*.txt` file contains documentation, processed through the system’s configured markup formatter (so may really be HTML, Markdown, etc., though the txt extension is required).

Jenkins is configured to see this shared library here: `Manage Jenkins » Configure System » Global Pipeline Libraries` 

See here for a complete explanation: https://jenkins.io/doc/book/pipeline/shared-libraries/ 
// Function that sends out an email notification at the start of a build.
def call(String notified, String jobName, String buildNumber, String nodeName, String buildUrl) {
    emailext (
        recipientProviders: [[$class: 'DevelopersRecipientProvider']],
        to: notified,
        mimeType: 'text/html',
        subject: "Jenkins Job ${jobName} Build ${buildNumber} Started",
        body: "<p>Jenkins Job ${jobName} Build ${buildNumber} has started on build server ${nodeName}.</p>" + 
            "<p>The build status is available immediately at this link: <a href=\'${buildUrl}\'>${jobName}</a></p>"
        )
}

// Function that report whether a build is a Release branch or not.
def call() {
    def rxRelease = env.BRANCH_NAME =~ /(?i)^release\//

    return !!rxRelease    
}

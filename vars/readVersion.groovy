// Function that reads a version number from the specified file.
def call(String versionFileName) {
    if (!versionFileName) {
        error('FATAL: readVersion was called with a null file name!')
    }
    else {
        String version

        sh "iconv -c -f utf8 -t ascii $versionFileName -o $versionFileName"
        String versionFileContents = readFile versionFileName
        def rxVersion

        // package.json parsing
        if (versionFileName.toLowerCase().endsWith('json')) {
            rxVersion = versionFileContents =~ /"version": "([^"]+)"/

            if (rxVersion) {
                version = rxVersion[0][1]
                echo "readVersion found version $version in file name ${versionFileName}"              
            }
            else {
                version = ''
                echo "WARNING: No Version Number found in project file: ${versionFileName}"
            }
        }
        // *proj parsing
        else if (versionFileName.toLowerCase().endsWith('proj')) {
            rxVersion = versionFileContents =~ /<Version>(.*)<\/Version>/

            if (rxVersion) {
                version = rxVersion[0][1]
                echo "readVersion found version $version in file name ${versionFileName}"               
            }
            else {
                version = ''
                echo "WARNING: No Version Number found in project file: ${versionFileName}"
            }
        }
        else {
            echo "WARNING: readVersion could not match file name ${versionFileName}!"
            version = ''
        }
        
        return version
    }
}

// Function that writes a Nuget.config file.
import globals

def call(String fileName, String proxy, String artifactoryNugetUrl) {
    writeFile file: "${fileName}", text: """<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <config>
        <add key="repositoryPath" value="%PACKAGEHOME%\\External" />
        <add key="defaultPushSource" value="https://${artifactoryNugetUrl}" />
        <add key="http_proxy" value="${proxy}" />
        <add key="https_proxy" value="${proxy}" />
    </config>

    <packageRestore>
        <!-- Allow NuGet to download missing packages -->
        <add key="enabled" value="True" />

        <!-- Automatically check for missing packages during build in Visual Studio -->
        <add key="automatic" value="True" />
    </packageRestore>
    <packageSources>
        <add key="Artifactory" value="https://${artifactoryNugetUrl}" />
        <add key="nuget.org" value="https://api.nuget.org/v3/index.json" protocolVersion="3" />
    </packageSources>

    <!-- Used to store credentials -->
    <packageSourceCredentials>
        <Artifactory>
            <add key="Username" value="${globals.artifactoryUserName}" />
            <add key="ClearTextPassword" value="${globals.artifactoryClearTextPassword}" />
        </Artifactory>
    </packageSourceCredentials>

    <!-- Used to disable package sources  -->
    <disabledPackageSources />

    <apikeys>
        <add key="https://${artifactoryNugetUrl}" value="${globals.artifactoryUserName}:${globals.artifactoryNugetPassword}" />
    </apikeys>
</configuration>"""
}

// Function that writes a Reports Index file.
def call(String fileName, String projectName, String fullVersionNumber, Map reports) {
    String friendlyDate = getFriendlyDate()
    String html = ""
    String icon = """
<div class="cards"><div class="card is-collapsed"><div class="card-inner"><svg-icon>
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511 511" style="enable-background:new 0 0 511 511;" xml:space="preserve">
<g><path d="M39.5,231h24c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-24c-4.142,0-7.5,3.358-7.5,7.5S35.358,231,39.5,231z"></path>
<path d="M39.5,167h168c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-168c-4.142,0-7.5,3.358-7.5,7.5S35.358,167,39.5,167z"></path>
<path d="M239.5,167h32c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-32c-4.142,0-7.5,3.358-7.5,7.5S235.358,167,239.5,167z"></path>
<path d="M375.5,167h96c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-96c-4.142,0-7.5,3.358-7.5,7.5S371.358,167,375.5,167z"></path>
<path d="M375.5,216c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3s0.8,3.91,2.2,5.3c1.39,1.4,3.33,2.2,5.3,2.2
c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3s-0.8-3.91-2.2-5.3C379.41,216.8,377.47,216,375.5,216z"></path>
<path d="M380.8,282.2c-1.39-1.4-3.33-2.2-5.3-2.2c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3s0.8,3.91,2.2,5.3
c1.39,1.4,3.33,2.2,5.3,2.2c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3S382.2,283.59,380.8,282.2z"></path>
<path d="M375.5,344c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3s0.8,3.91,2.2,5.3c1.39,1.4,3.33,2.2,5.3,2.2
c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3s-0.8-3.91-2.2-5.3C379.41,344.8,377.47,344,375.5,344z"></path>
<path d="M407.5,344c-1.98,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3s0.8,3.91,2.2,5.3c1.39,1.4,3.32,2.2,5.3,2.2
c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3s-0.8-3.91-2.2-5.3C411.41,344.8,409.48,344,407.5,344z"></path>
<path d="M375.5,408c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3s0.8,3.91,2.2,5.3c1.39,1.4,3.33,2.2,5.3,2.2
c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3s-0.8-3.91-2.2-5.3C379.41,408.8,377.47,408,375.5,408z"></path>
<path d="M407.5,408c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3s0.8,3.91,2.2,5.3c1.39,1.4,3.33,2.2,5.3,2.2
c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3s-0.8-3.91-2.2-5.3C411.41,408.8,409.47,408,407.5,408z"></path>
<path d="M444.8,346.2c-1.39-1.4-3.33-2.2-5.3-2.2c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.32-2.2,5.3s0.8,3.91,2.2,5.3
c1.39,1.4,3.33,2.2,5.3,2.2c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3S446.2,347.59,444.8,346.2z"></path>
<path d="M412.8,218.2c-1.39-1.4-3.32-2.2-5.3-2.2s-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3s0.8,3.91,2.2,5.3
c1.39,1.4,3.33,2.2,5.3,2.2c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3S414.2,219.59,412.8,218.2z"></path>
<path d="M63.5,408h-24c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h24c4.142,0,7.5-3.358,7.5-7.5S67.642,408,63.5,408z"></path>
<path d="M143.5,103h224c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-224c-4.142,0-7.5,3.358-7.5,7.5S139.358,103,143.5,103z"></path>
<path d="M39.5,295h24c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-24c-4.142,0-7.5,3.358-7.5,7.5S35.358,295,39.5,295z"></path>
<path d="M63.5,344h-24c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h24c4.142,0,7.5-3.358,7.5-7.5S67.642,344,63.5,344z"></path>
<path d="M215.5,344h-16c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h16c4.142,0,7.5-3.358,7.5-7.5S219.642,344,215.5,344z"></path>
<path d="M199.5,423h16c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-16c-4.142,0-7.5,3.358-7.5,7.5S195.358,423,199.5,423z"></path>
<path d="M215.5,280h-16c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h16c4.142,0,7.5-3.358,7.5-7.5S219.642,280,215.5,280z"></path>
<path d="M199.5,231h16c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-16c-4.142,0-7.5,3.358-7.5,7.5S195.358,231,199.5,231z"></path>
<path d="M247.5,231h32c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-32c-4.142,0-7.5,3.358-7.5,7.5S243.358,231,247.5,231z"></path>
<path d="M303.5,280h-56c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h56c4.142,0,7.5-3.358,7.5-7.5S307.642,280,303.5,280z"></path>
<path d="M263.5,344h-16c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h16c4.142,0,7.5-3.358,7.5-7.5S267.642,344,263.5,344z"></path>
<path d="M247.5,423h32c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-32c-4.142,0-7.5,3.358-7.5,7.5S243.358,423,247.5,423z"></path>
<path d="M471.5,56h-432C17.72,56,0,73.72,0,95.5v320C0,437.28,17.72,455,39.5,455h432c21.78,0,39.5-17.72,39.5-39.5v-320
C511,73.72,493.28,56,471.5,56z M175,376v-49h161v49H175z M336,391v49H175v-49H336z M496,135v49H351v-49H496z M336,199v49H175v-49
H336z M160,248H15v-49h145V248z M160,263v49H15v-49H160z M175,263h161v49H175V263z M351,263h145v49H351V263z M351,248v-49h145v49
H351z M336,184H15v-49h321V184z M15,327h145v49H15V327z M351,327h145v49H351V327z M39.5,71h432c13.509,0,24.5,10.991,24.5,24.5V120
H15V95.5C15,81.991,25.991,71,39.5,71z M15,415.5V391h145v49H39.5C25.991,440,15,429.009,15,415.5z M471.5,440H351v-49h145v24.5
C496,429.009,485.009,440,471.5,440z"></path></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
</svg-icon></div></div></div>
"""

    reports.each { key, value -> html += "<tr><td><a href='${value}'>${icon}</a></td><td><a href='${value}'>${key}</a></td></tr>" }

    writeFile file: "${fileName}", text: """<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Reports</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<link rel="icon" type="image/x-icon" href="favicon.ico">-->
    <link href="http://dhsrepo01.dhs.nycnet/reports/styles.2d72a43bc369c9811364.bundle.css" rel="stylesheet">
    <style>
        body {
            background: #fff
        }

        .navbar-search-result > div {
            overflow-y: scroll;
            z-index: 5;
            -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.3);
            box-shadow: 0 2px 5px 0 rgba(0,0,0,.3);
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            margin: 0 30%;
            padding: 0;
            background: #fff;
            width: auto;
            max-height: 50vh;
            height: auto;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column
        }

            .navbar-search-result > div.multi-column {
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row
            }

        .navbar-search-result .result-status {
            padding: 30px;
            text-align: center;
            font-size: 14px
        }

        .navbar-search-result .result-item {
            padding: 5px 30px
        }

            .navbar-search-result .result-item .result-item-heading {
                color: #343a40;
                font-size: 14px
            }

            .navbar-search-result .result-item p {
                color: #343a40;
                margin: 8px 0 8px 1em
            }

            .navbar-search-result .result-item a:hover {
                text-decoration: none
            }

            .navbar-search-result .result-item:focus, .navbar-search-result .result-item:hover {
                outline: 0;
                background-color: #d2d2d2
            }

            .navbar-search-result .result-item.active {
                background-color: #82abd2
            }
    </style>
    <style>
        caf-app-layout {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 100%;
            overflow: hidden
        }

            caf-app-layout .app-header {
                -webkit-box-flex: 0;
                -ms-flex: 0 0 auto;
                flex: 0 0 auto;
                height: auto;
                background: #243e53;
                z-index: 1000
            }

                caf-app-layout .app-header + .app-section, caf-app-layout .app-header + .app-section caf-app-layout .app-section {
                    height: calc(100% - 109px)
                }

        @media (min-width:768px) {
            caf-app-layout .app-header + .app-section, caf-app-layout .app-header + .app-section caf-app-layout .app-section {
                height: calc(100% - 54px)
            }
        }

        caf-app-layout .app-section {
            z-index: 0;
            -webkit-box-flex: 1;
            -ms-flex: 1 0 auto;
            flex: 1 0 auto;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row
        }

            caf-app-layout .app-section .app-left-sidebar, caf-app-layout .app-section .app-right-sidebar {
                z-index: 1000
            }

            caf-app-layout .app-section > div:nth-child(0) {
                -webkit-box-ordinal-group: 1;
                -ms-flex-order: 0;
                order: 0
            }

            caf-app-layout .app-section > div:nth-child(1) {
                -webkit-box-ordinal-group: 2;
                -ms-flex-order: 1;
                order: 1
            }

            caf-app-layout .app-section > div:nth-child(2) {
                -webkit-box-ordinal-group: 3;
                -ms-flex-order: 2;
                order: 2
            }

            caf-app-layout .app-section > div:nth-child(3) {
                -webkit-box-ordinal-group: 4;
                -ms-flex-order: 3;
                order: 3
            }

        caf-app-layout .app-content {
            position: relative;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            overflow-y: auto
        }

            caf-app-layout .app-content > .content {
                margin-left: 0
            }
    </style>
    <style>
        [_nghost-c2] {
            position: relative;
            display: block
        }

        .loading-bar-fixed[_nghost-c2] > div[_ngcontent-c2] .bar[_ngcontent-c2] {
            position: fixed
        }

        .loading-bar-fixed[_nghost-c2] > div[_ngcontent-c2]:first-child {
            position: fixed;
            top: 10px;
            left: 10px
        }

        .loading-bar-fixed[_nghost-c2] > div[_ngcontent-c2] .peg[_ngcontent-c2] {
            display: block
        }

        [_nghost-c2] > div[_ngcontent-c2] {
            pointer-events: none;
            -webkit-transition: 350ms linear all;
            transition: 350ms linear all;
            color: #29d
        }

            [_nghost-c2] > div[_ngcontent-c2] .bar[_ngcontent-c2] {
                -webkit-transition: width 350ms;
                transition: width 350ms;
                background: #29d;
                position: absolute;
                z-index: 10002;
                top: 0;
                left: 0;
                width: 100%;
                height: 2px;
                border-bottom-right-radius: 1px;
                border-top-right-radius: 1px
            }

            [_nghost-c2] > div[_ngcontent-c2] .peg[_ngcontent-c2] {
                display: none;
                position: absolute;
                width: 70px;
                right: 0;
                top: 0;
                height: 2px;
                opacity: .45;
                -webkit-box-shadow: 1px 0 6px 1px;
                box-shadow: 1px 0 6px 1px;
                color: inherit;
                border-radius: 100%
            }

            [_nghost-c2] > div[_ngcontent-c2]:first-child {
                display: block;
                position: absolute;
                z-index: 10002;
                top: 5px;
                left: 0
            }

                [_nghost-c2] > div[_ngcontent-c2]:first-child .spinner-icon[_ngcontent-c2] {
                    width: 14px;
                    height: 14px;
                    border: 2px solid transparent;
                    border-top-color: inherit;
                    border-left-color: inherit;
                    border-radius: 50%;
                    -webkit-animation: .4s linear infinite loading-bar-spinner;
                    animation: .4s linear infinite loading-bar-spinner
                }

        @-webkit-keyframes loading-bar-spinner {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        @keyframes loading-bar-spinner {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }
    </style>
    <style>
        caf-navbar {
            display: block
        }

            caf-navbar > nav.navbar {
                padding: 0
            }

            caf-navbar caf-menu label > span > i.fa, caf-navbar caf-menu-item a > i.fa {
                margin-right: 6px
            }

            caf-navbar .nav.navbar-nav > div {
                float: left
            }

                caf-navbar .nav.navbar-nav > div > a {
                    line-height: 20px;
                    padding: 17px 15px;
                    position: relative;
                    display: block
                }

                    caf-navbar .nav.navbar-nav > div > a:focus, caf-navbar .nav.navbar-nav > div > a:hover {
                        background: 0 0;
                        opacity: .6;
                        filter: alpha(opacity=60);
                        text-decoration: none;
                        color: #333
                    }

            caf-navbar .nav.navbar-nav .dropdown-menu {
                margin-top: 0;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
                position: absolute
            }

            caf-navbar .nav.navbar-nav .dropdown-toggle:after {
                border-top: .3em solid #fff
            }

            caf-navbar .nav.navbar-nav > a, caf-navbar .nav.navbar-nav > caf-menu label, caf-navbar .nav.navbar-nav > caf-menu-item {
                cursor: pointer
            }

                caf-navbar .nav.navbar-nav > a:hover, caf-navbar .nav.navbar-nav > caf-menu label:hover, caf-navbar .nav.navbar-nav > caf-menu-item:hover {
                    opacity: .6
                }

            caf-navbar .nav.navbar-nav > caf-menu-item {
                padding: 12px 15px
            }

                caf-navbar .nav.navbar-nav > a, caf-navbar .nav.navbar-nav > caf-menu-item a {
                    background: 0 0;
                    color: #fff;
                    font-weight: 600
                }

                    caf-navbar .nav.navbar-nav > a:hover, caf-navbar .nav.navbar-nav > caf-menu-item a:hover {
                        color: #fff
                    }

            caf-navbar .navbar-collapse {
                display: none;
                -ms-flex-preferred-size: auto;
                flex-basis: auto
            }

                caf-navbar .navbar-collapse > .nav.navbar-nav {
                    -webkit-box-orient: horizontal;
                    -webkit-box-direction: normal;
                    -ms-flex-direction: row;
                    flex-direction: row;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center
                }

            caf-navbar .navbar-form {
                margin: 12px 6px;
                position: relative
            }

        @media (max-width:767px) {
            caf-navbar .navbar-form {
                margin: 12px 0;
                padding: inherit
            }
        }

        caf-navbar .navbar-form .btn-search {
            position: absolute;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            top: 0;
            right: 0
        }

        caf-navbar button.navbar-toggler {
            color: #fff;
            display: block
        }

            caf-navbar button.navbar-toggler:focus {
                outline: 0
            }

        caf-navbar .navbar-right {
            width: 100%;
            border-top: 1px solid #fff;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row
        }

        @media (min-width:768px) {
            caf-navbar .navbar-collapse {
                display: block
            }

            caf-navbar button.navbar-toggler {
                display: none
            }

            caf-navbar .navbar-right {
                width: auto;
                border-top: none
            }
        }

        caf-navbar > nav.navbar.navbar-default {
            background: #263e53;
            -webkit-transition: background-color .2s linear;
            transition: background-color .2s linear;
            z-index: 10
        }

        caf-navbar .navbar-search-result {
            z-index: 0
        }

        .navbar-search-result > div {
            overflow-y: scroll;
            z-index: 5;
            -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.3);
            box-shadow: 0 2px 5px 0 rgba(0,0,0,.3);
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            margin: 0 30%;
            padding: 0;
            background: #fff;
            width: auto;
            max-height: 50vh;
            height: auto;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column
        }

            .navbar-search-result > div.multi-column {
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row
            }

        .navbar-search-result .result-status {
            padding: 30px;
            text-align: center;
            font-size: 14px
        }

        .navbar-search-result .result-item {
            padding: 5px 30px
        }

            .navbar-search-result .result-item .result-item-heading {
                color: #343a40;
                font-size: 14px
            }

            .navbar-search-result .result-item p {
                color: #343a40;
                margin: 8px 0 8px 1em
            }

            .navbar-search-result .result-item a:hover {
                text-decoration: none
            }

            .navbar-search-result .result-item:focus, .navbar-search-result .result-item:hover {
                outline: 0;
                background-color: #d2d2d2
            }

            .navbar-search-result .result-item.active {
                background-color: #82abd2
            }
    </style>
    <style>
        caf-autocomplete {
            position: relative;
            display: block
        }

            caf-autocomplete button.btn-search {
                position: absolute;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                top: 0;
                right: 0;
                background: 0 0;
                outline: 0;
                height: 100%
            }

                caf-autocomplete button.btn-search:focus {
                    outline: 0
                }

            caf-autocomplete.navbar-form .form-control.open {
                border-bottom-left-radius: 30px;
                border-bottom-right-radius: 30px
            }

            caf-autocomplete .form-control.open {
                border-bottom-left-radius: 0;
                border-bottom-right-radius: 0
            }

        .auto-complete.dropdown-menu {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            position: inherit;
            width: 100%;
            border-top: 1px solid #eee;
            margin-top: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0
        }

        .dropdown-item {
            padding: 0
        }

            .dropdown-item a {
                padding: .25rem 1.5rem
            }

        .auto-complete-overlay-panel {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            overflow: auto;
            padding: 68px 32px 0;
            color: #fafafa;
            width: auto;
            max-height: 95vh;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 5;
            background-color: #333;
            -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.3);
            box-shadow: 0 2px 5px 0 rgba(0,0,0,.3);
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            height: 70vh
        }
    </style>
    <style>
        .navbar .nav [dropdowntoggle] {
            height: 54px
        }

        .nav.navbar-nav > caf-menu, .nav.navbar-nav > div {
            display: inline-block
        }

        caf-menu {
            display: block
        }

            caf-menu.dropup > .dropdown-menu {
                border-bottom: 1px solid #eee;
                margin-bottom: 0;
                border-bottom-left-radius: 0;
                border-bottom-right-radius: 0
            }

            caf-menu.dropdown > .dropdown-menu {
                border-top: 1px solid #eee;
                margin-top: 0;
                border-top-left-radius: 0;
                border-top-right-radius: 0
            }

            caf-menu.dropdown > .dropdown-toggle {
                display: -webkit-inline-box;
                display: -ms-inline-flexbox;
                display: inline-flex;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                padding: 12px 15px;
                margin: 0
            }

                caf-menu.dropdown > .dropdown-toggle img {
                    float: left;
                    width: 30px;
                    height: 30px;
                    margin: -5px 10px -5px 0;
                    border-radius: 30px
                }

            caf-menu > .dropdown-menu {
                height: -webkit-max-content;
                height: -moz-max-content;
                height: max-content
            }

                caf-menu > .dropdown-menu.media-list p {
                    margin-bottom: 4px;
                    max-width: 200px
                }

                caf-menu > .dropdown-menu > .dropdown-item {
                    display: block;
                    width: 100%;
                    padding: .25rem 0;
                    clear: both;
                    font-weight: 400;
                    color: #212529;
                    text-align: inherit;
                    white-space: nowrap;
                    background-color: transparent;
                    border: 0
                }

            caf-menu .media-body, caf-menu .media-left, caf-menu .media-right {
                display: table-cell;
                vertical-align: middle
            }
    </style>
    <style>
        caf-menu-item {
            display: list-item;
            text-align: -webkit-match-parent;
            list-style-type: none
        }

            caf-menu-item:focus, caf-menu-item:hover {
                outline: 0
            }

            caf-menu-item > a:focus, caf-menu-item > a:hover {
                background: #edf0f5;
                text-decoration: none;
                outline: 0
            }

            caf-menu-item > .media {
                margin-top: 0;
                border-top: 1px solid #eee;
                border-bottom: 1px solid #eee;
                margin-bottom: -1px
            }

                caf-menu-item > .media > a {
                    display: block;
                    padding: 10px 20px !important
                }

                caf-menu-item > .media .media-object {
                    height: 36px;
                    width: 36px;
                    line-height: 36px;
                    font-size: 14px;
                    color: #fff;
                    text-align: center;
                    margin-right: 10px;
                    border-radius: 50%
                }

            caf-menu-item > li.dropdown-footer > a {
                padding: 0 !important;
                display: inline !important
            }

                caf-menu-item > li.dropdown-footer > a:focus, caf-menu-item > li.dropdown-footer > a:hover {
                    background: 0 0 !important;
                    text-decoration: underline !important
                }

        .dropdown-menu > caf-menu-item > a {
            display: block;
            padding: 5px 15px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap
        }

        .dropdown-menu.media-list .dropdown-header + .media {
            margin-top: -10px
        }
    </style>
    <style>
        [_nghost-c9] {
            background: #fff;
            position: relative;
            display: block
        }

            [_nghost-c9] .background-sky[_ngcontent-c9] {
                background-color: #1976d2;
                background: linear-gradient(145deg,#0d47a1,#42a5f5);
                min-height: 410px;
                height: 55vh;
                max-height: 560px;
                -webkit-transform: skewY(8deg);
                transform: skewY(8deg);
                -webkit-transform-origin: 100%;
                transform-origin: 100%;
                width: 100%;
                position: absolute;
                top: 0;
                overflow: hidden
            }

            [_nghost-c9] #intro.jumbotron[_ngcontent-c9] {
                background: 0 0;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                height: 20vh;
                width: 100%;
                max-width: 100vw;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column;
                z-index: 100;
                padding: unset
            }

                [_nghost-c9] #intro.jumbotron[_ngcontent-c9] .sub-heading[_ngcontent-c9] {
                    font-size: 20px;
                    color: #fff;
                    font-weight: 100;
                    letter-spacing: 3px;
                    margin-bottom: 20px;
                    z-index: 100
                }

                [_nghost-c9] #intro.jumbotron[_ngcontent-c9] .logo[_ngcontent-c9] {
                    margin-left: 32px
                }

                [_nghost-c9] #intro.jumbotron[_ngcontent-c9] button[_ngcontent-c9] {
                    z-index: 100;
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center;
                    -webkit-box-pack: center;
                    -ms-flex-pack: center;
                    justify-content: center;
                    width: 184px;
                    height: 40px;
                    padding: 0 24px;
                    font-size: 18px;
                    font-weight: 600;
                    line-height: 40px;
                    background-color: #fff;
                    border-radius: 48px;
                    -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.26);
                    box-shadow: 0 2px 5px 0 rgba(0,0,0,.26);
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box;
                    cursor: pointer;
                    color: #1976d2
                }

                    [_nghost-c9] #intro.jumbotron[_ngcontent-c9] button[_ngcontent-c9]:hover {
                        opacity: .9;
                        color: #6e6e6e
                    }

            [_nghost-c9] .logo[_ngcontent-c9] span[_ngcontent-c9] {
                -webkit-transform: translate3d(-12px,12px,0);
                transform: translate3d(-12px,12px,0);
                color: #646464;
                mix-blend-mode: screen;
                font-family: Montserrat,sans-serif;
                font-size: 12em;
                letter-spacing: .2em;
                text-transform: uppercase;
                text-shadow: 0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff,0 0 #fff;
                opacity: 0;
                -webkit-animation: 2s .5s forwards revolve;
                animation: 2s .5s forwards revolve
            }

        @-webkit-keyframes revolve {
            to {
                opacity: 1;
                -webkit-transform: translate3d(0,0,0);
                transform: translate3d(0,0,0);
                text-shadow: -2px 1px #fff,-3px 2px #fff,-4px 3px #fff,-5px 4px #fff,-6px 5px #fff,-7px 6px #fff,-8px 7px #fff,-9px 8px #fff,-10px 9px #fff,-11px 10px #fff,-12px 11px #fff
            }
        }

        @keyframes revolve {
            to {
                opacity: 1;
                -webkit-transform: translate3d(0,0,0);
                transform: translate3d(0,0,0);
                text-shadow: -2px 1px #fff,-3px 2px #fff,-4px 3px #fff,-5px 4px #fff,-6px 5px #fff,-7px 6px #fff,-8px 7px #fff,-9px 8px #fff,-10px 9px #fff,-11px 10px #fff,-12px 11px #fff
            }
        }
    </style>
    <style>
        doc-package-cards .header {
            padding: 30px 30px 0;
            text-align: center
        }

            doc-package-cards .header > header-title {
                margin: 0;
                text-transform: uppercase;
                font-size: 2.5em;
                font-weight: 500;
                line-height: 1.1
            }

            doc-package-cards .header > header-subtitle {
                margin: 0;
                font-size: 1.5em;
                color: #333;
                font-weight: 500;
                line-height: 1.1
            }

        .cards {
            padding: 15px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-flow: row wrap;
            flex-flow: row wrap
        }

            .card {
                position: unset;
                margin: 15px;
                width: calc((100% / 6) - 30px);
                -webkit-transition: all .2s ease-in-out;
                transition: all .2s ease-in-out;
                border: none
            }

        @media screen and (max-width:991px) {
            .card {
                width: calc((100% / 4) - 30px)
            }
        }

        @media screen and (max-width:767px) {
            .card {
                width: calc((100% / 2) - 30px)
            }
        }

        @media screen and (max-width:300px) {
            .card {
                width: 100%
            }
        }

        .card:hover .car-inner {
            color: #263e53;
            -webkit-transform: scale(1.05);
            transform: scale(1.05)
        }

        .card > .card-inner {
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            width: 100%;
            position: relative;
            cursor: pointer;
            color: #4c4c4c;
            font-size: 1.5em;
            text-transform: uppercase;
            text-align: center;
            z-index: 2;
            -webkit-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out
        }

            .card > .card-inner:focus, .card > .card-inner:hover {
                outline: 0
            }

            .card > .card-inner h4 {
                font-size: 1rem
            }

            .card > .card-inner .fa {
                width: 100%;
                margin-top: .25em
            }

            .card > .card-inner svg-icon > svg {
                fill: #646464
            }

                .card > .card-inner svg-icon > svg:hover {
                    -webkit-filter: drop-shadow(3px 3px 2px rgba(0, 0, 0, .2));
                    filter: drop-shadow(3px 3px 2px rgba(0, 0, 0, .2))
                }

        .card .card-expander {
            padding: 20px 10px;
            border: 0 solid #e6e7e8;
            border-top-width: 2px;
            border-bottom-width: 2px;
            background-color: #fafafa;
            -webkit-transition: height .24s ease-out;
            transition: height .24s ease-out;
            position: relative
        }

            .card .card-expander .module-content {
                padding: 0 20px 24px
            }

                .card .card-expander .module-content a {
                    display: block;
                    padding-bottom: 16px;
                    font-size: 14px;
                    color: #333;
                    line-height: 1.5;
                    text-shadow: #fff 0 1px 0;
                    cursor: pointer
                }

                .card .card-expander .module-content span {
                    display: block;
                    font-size: 13px;
                    line-height: 1.5;
                    color: #828282;
                    letter-spacing: .03em
                }

            .card .card-expander .fa {
                font-size: .75em;
                position: absolute;
                top: 10px;
                right: 10px;
                cursor: pointer
            }

                .card .card-expander .fa:hover {
                    opacity: .9
                }

        .card.is-collapsed .card-inner .card-arrow {
            opacity: 0
        }

        .card.is-collapsed .card-expander {
            max-height: 0;
            min-height: 0;
            overflow: hidden;
            margin-top: 0;
            opacity: 0
        }

        .card.is-expanded .card-inner .card-arrow, .card.is-expanded .card-inner .card-arrow-inner {
            opacity: 1;
            display: block;
            position: absolute;
            bottom: -30px;
            left: calc(50% - 15px);
            width: 0;
            height: 8px;
            line-height: 0;
            font-size: 0;
            border-style: solid;
            border-width: 0 8px 8px;
            border-color: transparent;
            z-index: 3
        }

        .card.is-expanded .card-inner .card-arrow {
            border-bottom: 8px solid #e6e7e8
        }

        .card.is-expanded .card-inner .card-arrow-inner {
            top: 3px;
            left: -8px;
            border-bottom: 8px solid #fafafa
        }

        .card.is-expanded .card-inner .fa:before {
            content: "\f115"
        }

        .card.is-expanded .card-expander {
            max-height: 1000px;
            min-height: 100px;
            overflow: visible;
            margin-top: 30px;
            opacity: 1;
            z-index: 1
        }

        .card.is-expanded:hover .card-inner {
            -webkit-transform: scale(1);
            transform: scale(1)
        }

        .card.is-inactive .card-inner {
            opacity: .5
        }

        .card.is-inactive:hover .card-inner {
            color: #263e53;
            -webkit-transform: scale(1);
            transform: scale(1)
        }

        @media screen and (min-width:992px) {
            .card:nth-of-type(6n+2) .card-expander {
                margin-left: calc(-100% - 30px)
            }

            .card:nth-of-type(6n+3) .card-expander {
                margin-left: calc(-200% - 60px)
            }

            .card:nth-of-type(6n+4) .card-expander {
                margin-left: calc(-300% - 90px)
            }

            .card:nth-of-type(6n+5) .card-expander {
                margin-left: calc(-400% - 120px)
            }

            .card:nth-of-type(6n+6) .card-expander {
                margin-left: calc(-500% - 150px)
            }

            .card:nth-of-type(6n+7) {
                clear: left
            }

            .card-expander {
                width: calc(600% + 150px)
            }
        }

        @media screen and (min-width:768px) and (max-width:991px) {
            .card:nth-of-type(4n+2) .card-expander {
                margin-left: calc(-100% - 30px)
            }

            .card:nth-of-type(4n+3) .card-expander {
                margin-left: calc(-200% - 60px)
            }

            .card:nth-of-type(4n+4) .card-expander {
                margin-left: calc(-300% - 90px)
            }

            .card:nth-of-type(4n+5) {
                clear: left
            }

            .card-expander {
                width: calc(400% + 90px)
            }
        }

        @media screen and (min-width:301px) and (max-width:767px) {
            .card:nth-of-type(2n+2) .card-expander {
                margin-left: calc(-100% - 30px)
            }

            .card:nth-of-type(2n+3) {
                clear: left
            }

            .card-expander {
                width: calc(200% + 30px)
            }
        }
    </style>
    <style>
        :host {
            display: inline-block;
        }

        td {
            font-size: 16px;
            color: #000;
        }
    </style>
</head>
<body>
    <doc-root ng-version="5.2.11">
        <caf-app-layout class="root">
            <!----><div class="app-header">
                <!---->
                <doc-header>
                    <caf-navbar>
                        <nav class="navbar navbar-default">
                            <a class="navbar-brand" routerlink="/" href="/">
                                <img alt="logo" src="http://dhsrepo01.dhs.nycnet/reports/dss_logo_white_sm.png" width="130">
                            </a>
                            <!----><button class="navbar-toggler" type="button">
                                <span class="fa fa-bars"></span>
                            </button>
                            <!----><div class="navbar-collapse collapse show">
                                <div class="nav navbar-nav">
                                    <!---->
                                    <div tabindex="0">
                                        <!---->
                                        <span style="color: #fff;font-size: 18px;font-weight: 700;">
                                            CAF Application Reports
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </nav>
                        <!----><div class="navbar-search-result">
                        </div>
                    </caf-navbar>
                </doc-header>
            </div>
            <div class="app-section">
                <!---->
                <!----><div cafscrollable="" class="app-content">
                    <!---->
                    <router-outlet></router-outlet><doc-home _nghost-c9="">
                        <div _ngcontent-c9="" class="background-sky"></div>
                        <section _ngcontent-c9="" class="jumbotron rounded-0" id="intro">
                            <div _ngcontent-c9="" class="logo">
                                <span _ngcontent-c9="">CAF</span>
                            </div>
                        </section>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="header">
                                        <h1 class="header-title">CAF Reports for ${projectName} Version ${fullVersionNumber}</h1>
                                        <h2 class="header-subtitle">Build Date: ${friendlyDate}</h2>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <table class="table table-condensed table-striped">
                                        <tr><th colspan="2">Reports</th></tr>
                                        ${html}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </doc-home>
                </div>
            </div>
        </caf-app-layout>
        <caf-loading-bar _nghost-c2="" class="loading-bar-fixed"><!----></caf-loading-bar>
    </doc-root>
    <!--<script type="text/javascript" src="inline.318b50c57b4eba3d437b.bundle.js"></script>
    <script type="text/javascript" src="polyfills.9396d08b50390e47e20e.bundle.js"></script>
    <script type="text/javascript" src="scripts.77eb60f19351cd754332.bundle.js"></script>
    <script type="text/javascript" src="main.37677395aa2d3d20520f.bundle.js"></script>-->
</body>
</html>
"""
}

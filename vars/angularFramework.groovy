// This Jenkinsfile library is for CI/CD of Frameworks built with Angular.
import groovy.transform.TypeChecked
import globals

@TypeChecked
def call(Map parms) {
    // Project Definition Parameters

    // nodeLabel controls which Jenkins slaves this project can build on, and is required.
    String nodeLabel = required value: parms.get('nodeLabel'), message: "'nodeLabel' parameter not set in the Jenkinsfile.  This parameter is required."

    // versionFile specifies a file that contains the version number that will be used for the entire project package, and is required.
    String versionFile = required value: parms.get('versionFile'), message: "'versionFile' parameter not set in the Jenkinsfile.  This parameter is required."

    // projectName specifies a string that uniquely identifies the project, and is used to build several output filenames.  This is required.
    String projectName = required value: parms.get('projectName'), message: "'projectName' parameter not set in the Jenkinsfile.  This parameter is required."
    
    // projectFile specifies a project file to be passed as a parameter to the build command, and is required when the main project folder contains more than one project or solution file.
    String projectFile = parms.get("projectFile", '')

    // artifactoryPublishRepo specifies the folder within Artifactory to deploy the build package to, and is required.
    String artifactoryPublishRepo = required value: parms.get('artifactoryPublishRepo'), message: "Parameter 'artifactoryPublishRepo' was not defined - this is required to indicate the publish destination in Artifactory."
    String artifactoryPublishUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${artifactoryPublishRepo}/"
    String artifactoryPublishDocsUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/nuget/builds/${projectName}/"

    // artifactoryNpmRepo specifies the folder within Artifactory to source npm packages from.
    String artifactoryNpmRepo = parms.get("artifactoryNugetRepo", globals.artifactoryNpmRepo)
    String artifactoryNpmUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/${artifactoryNpmRepo}"

    // Process Control Parameters (can be set to false to skip for faster builds)
    // init (always executes): - Setup variables and print out values for debugging
    String initCommand = parms.get("initCommand", "")

    // install-dependencies-npm:	configure artifactory for pull and push of npm, npm install
    Boolean installDependencies = parms.get("installDependencies", false)
    String installDependenciesCommand = parms.get("installDependenciesCommand", "")

    // build-npm:   npm run build
    Boolean npmBuild = parms.get("npmBuild", false)
    Boolean docBuild = parms.get("docBuild", false)
    Boolean docBundleServer = parms.get("docBundleServer", false)
    Boolean copyServer = parms.get("copyServer", false)
    Boolean copyClient = parms.get("copyClient", false)

    // test-npm:	run npm unit tests and feature tests and generates the reports
    Boolean npmLint = parms.get("npmLint", false)
    String lintCommand = parms.get("lintCommand", "")
    Boolean npmTest = parms.get("npmTest", false)
    String testCommand = parms.get("testCommand", "")
    Boolean npmCoverage = parms.get("npmCoverage", false)
    String coverageCommand = parms.get("coverageCommand", "")
    Boolean runNpmTests = npmLint || npmTest || npmCoverage
    String testCommandLine = "echo " +
        (npmLint ? "&& npm run lint${lintCommand}" : "") +
        (npmTest ? "&& npm run test${testCommand}" : "") +
        (npmCoverage ? "&& npm run coverage${coverageCommand}" : "")

    String testProjectFolder = parms.get("testProjectFolder", '')
    
    // copy-reports-dotnet:	copy all test reports to report server    
    Boolean testReports = parms.get("testReports", false)
    String reportFolder = parms.get("reportFolder", "")
    String reportServerPhysicalRoot = "${globals.reportServerPhysicalRoot}/${reportFolder}"
    Boolean coverageReports = parms.get("coverageReports", false)
    String coverageDirectory = parms.get("coverageDirectory", "")
    Boolean runReports
    String reportsCommandLine
    String reportUrl
    def reportsMap = [:]

    // notify	(always executes): send emails to "notified" folks and send slack msg to slack channel
    String notified = parms.get("notified", "")

    Boolean cleanWorkspaceAfterRun = parms.get("cleanWorkspaceAfterRun", false)

    // init Phase 1 (always executes): - Setup variables and print out values for debugging
    String publishFolder = parms.get("publishFolder", "publish")
    String publishPhysicalFolder

    String branchName
    String buildNumber

    String version
    String versionSuffix

    String nodeName

    String workspaceFolder

    String jobName
    String buildUrl
    String buildUrlSlackMessage
    String buildUrlHtmlMessage
    String octopusChannelId

    String dateBuildNumber
    String fullVersionNumber = ""
    String notificationHtmlFooter = ''
    String notificationSlackFooter = ''
   
    String nupkgOctopusProjectId = parms.get("nupkgOctopusProjectId", "")
    String nupkgOctopusStepName = parms.get("nupkgOctopusStepName", "")
    String nupkgOctopusActionName = parms.get("nupkgOctopusActionName", "")
    String nupkgOctopusUnstableChannelId = parms.get("nupkgOctopusUnstableChannelId", "")
    String nupkgOctopusDefaultChannelId = parms.get("nupkgOctopusDefaultChannelId", "Default")

    String slackChannel = parms.slackChannel

    String npmRepoName = required value: parms.get('npmRepoName'), message: "Parameter 'npmRepoName' was not defined - this is required to indicate the npm source in Artifactory."
    String npmArtifactoryUrl = "${globals.artifactoryRoot}/${globals.artifactoryServerName}/api/npm/${npmRepoName}"
    String npmScope = required value: parms.get('npmScope'), message: "Parameter 'npmScope' was not defined - this is required."
    Boolean npmClean = parms.get("npmClean", false)
    Boolean npmPackage = parms.get("npmPackage", false)
    String packageCommand = parms.get("packageCommand", "")
    Boolean npmPublish = parms.get("npmPublish", false)
    String npmPublishCommand = parms.get("npmPublishCommand", "")
    Boolean isPR = isPullRequest()
    Boolean isRel = isRelease()

    // Define the source files folder.
    String sourceFolder = parms.get("sourceFolder", ".")

    // Define the libs folder.
    String libsFolder = parms.get("libsFolder", "libs")

    // Define the documentation source files folder.
    String docsFolder = parms.get("docsFolder", ".")

    // Define the documentation files folder.
    String docFilesFolder = parms.get("docFilesFolder", ".")

    // Define the documentation server files folder.
    String docServerFolder = parms.get("docServerFolder", ".")

/*

    // Process control

    String publishFolder = parms.get("publishFolder", "")
    String publishFolderPathSegment = publishFolder ? publishFolder + '/' : ''

    Boolean nugetPublish = parms.get("nugetPublish", false)

    Boolean deployArtifacts = parms.get("deployArtifacts", false)
    String deployCommand = parms.get("deployCommand", "")

    // Define pipeline global variables.
    // TODO: Throw errors if required parms not set.
    String versionFile = parms.versionFile

    String artifactoryZipFileBaseName = parms.get("artifactoryZipFileBaseName", "")
    String artifactorySourceFolder = parms.get("artifactorySourceFolder", "")
    String artifactoryZipOctopusProjectId = parms.get("artifactoryZipOctopusProjectId", "")
    String artifactoryZipOctopusStepName = parms.get("artifactoryZipOctopusStepName", "")
    String artifactoryZipOctopusActionName = parms.get("artifactoryZipOctopusActionName", "")

    String artifactoryNugetRepo = parms.get("artifactoryNugetRepo", "")
    if (dotnetBuild && !artifactoryNugetRepo){
        error("'artifactoryNugetRepo' parameter not set when dotnet build has been enabled!")
    }
    String artifactoryNugetUrl = "${artifactoryRoot}/${artifactoryServerName}/api/${artifactoryNugetRepo}/"

    String nugetBaseFileName = parms.get("nugetBaseFileName", "")
    String nuspecBaseFileName = parms.get("nuspecBaseFileName", "")
    String nuspecFolder = parms.get("nuspecFolder", ".")
    String nuspecAuthor = parms.get("nuspecAuthor", "")
    String nupkgFileName
    String nuspecFileName
*/
    Boolean nugetDeploy = parms.get("nugetDeploy", false)
    
pipeline {
    agent {
        node {
            label nodeLabel
            customWorkspace URLDecoder.decode("/home/jenkins/workspace/${JOB_NAME}", "UTF-8")
        }    
    }
    stages {
        // Initialize pipeline.
        // Environment has been set up by now, so it is safe to set variables from it.
        stage('init') {
            steps {
                script {
                    Boolean abortedMasterBuild = false

                    try {
                        // Set proxies in the environment for dotnet.
                        env.http_proxy = globals.http_proxy
                        env.https_proxy = globals.https_proxy
                        env.NPM_SCOPE = "${npmScope}"

                        // Execute any initCommand defined for the job.
                        if (initCommand != '') {
                            sh initCommand
                        }

                        // Get environment values set by Jenkins.
                        branchName = "${env.BRANCH_NAME}"
                        buildNumber = "${env.BUILD_NUMBER}"
                        jobName = "${env.JOB_NAME}"
                        nodeName = "${env.NODE_NAME}"
                        workspaceFolder = "${env.WORKSPACE}"
                        buildUrl = "${env.RUN_DISPLAY_URL}"
                        buildUrlSlackMessage = "Click <${buildUrl}|here> to view job details"
                        buildUrlHtmlMessage = "<p>Click <a href=\'${buildUrl}\'>here</a> to view job details.</p>"

                        // Prevent builds against master.
                        if (branchName == 'master') {
                            abortedMasterBuild = true
                            echo "Attempt to build from MASTER branch - aborting..."
                            error("Attempt to build from MASTER branch - aborting...")
                        }

                        if (npmClean) {
                            echo "clean npm modules"
                            sh "rm -rf node_modules"
                        }

                        // Read the version from the version file.
                        version = readVersion versionFile
                       
                        // Generate unique date / build stamp for file names
                        dateBuildNumber = buildSerialNumber buildNumber

                        // Generate the full version number.
                        fullVersionNumber = generateVersionNumber version, branchName, dateBuildNumber

                        // Extract the version suffix.
                        versionSuffix = fullVersionNumber.substring(version.length());

                        // Update project version numbers.
                        if (versionSuffix != '') {
                            // Update the project version in the root project.
                            updateProjectVersions versionSuffix, false

                            // Update the project versions in the sourceFolder folder.
                            updateProjectVersions versionSuffix, true, sourceFolder

                            // Update the project versions in the libs folder.
                            updateProjectVersions versionSuffix, true, libsFolder

                            // Update the project versions in the docsFolder folder.
                            updateProjectVersions versionSuffix, true, docsFolder

                            // Update the project versions in the doc-server folder.
                            updateProjectVersions versionSuffix, false, docServerFolder
                        }

                        reportUrl = "reports/${reportFolder}/${dateBuildNumber}"    

                        // get the channelID
                        octopusChannelId = branchName =~ /^(feature\/|develop)/ ? "${nupkgOctopusUnstableChannelId}": "${nupkgOctopusDefaultChannelId}"


                    String createFolderCommand = "sshpass -p '${globals.reportServerPassword}' ssh -p 22 ${globals.reportServerUser} mkdir -p ${reportServerPhysicalRoot}"
                    String publishTestReports = "&& sshpass -p '${globals.reportServerPassword}' rsync -r reports/ ${globals.reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"
                    String publishCoverageReports = "&& sshpass -p '${globals.reportServerPassword}' rsync -r coverage ${globals.reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"

                    runReports = testReports || coverageReports
                    reportsCommandLine = createFolderCommand +
                        (testReports ? publishTestReports : "") +
                        (coverageReports ? publishCoverageReports : "")

                    // If runReports is true but no reportFolder was specified, thow an error.
                    if (runReports && reportFolder == '') {
                        error("runReports is true but no reportFolder was specified - please specify the reportFolder parameter.")
                        currentBuild.result = 'ABORTED'
                    }

                    notificationHtmlFooter = runReports? "<p>Reports are available at <a href=\'http://${globals.reportServer}/${reportUrl}/\'>View Reports</a></p>" : ""
                    notificationSlackFooter = runReports ? "\nReports are available at <http://${globals.reportServer}/${reportUrl}|View Reports>" : ""

                    // nupkgFileName = "${nugetBaseFileName}.${fullVersionNumber}.nupkg"
                    // nuspecFileName = "${nuspecBaseFileName}.${fullVersionNumber}.nuspec"

                    // If the project uses npm, configure it for the job.
                    if (installDependencies || npmBuild || runNpmTests || npmPackage || npmPublish) {
                        echo "Configure npm to use Artifactory"
                        sh "npm config set ${npmScope}:registry https://${npmArtifactoryUrl}/"

                        sh "npm config set //${npmArtifactoryUrl}:_password ${globals.artifactoryNpmPassword}"
                        sh "npm config set //${npmArtifactoryUrl}:username ${globals.artifactoryUserName}"
                        sh "npm config set //${npmArtifactoryUrl}:email ${globals.artifactoryEmail}"
                        sh "npm config set //${npmArtifactoryUrl}:always-auth true"
                    }

/*
                        // Configure reporting variables
                        String reportServerReportPhysicalRoot = "${reportServerPhysicalRoot}/${fullVersionNumber}"
                        String createReportFolderCommand = "sshpass -p '${globals.reportServerPassword}' ssh -p 22 ${globals.reportServerUser} mkdir -p ${reportServerReportPhysicalRoot}/unit-tests"
                        String publishTestReports = "&& sshpass -p '${globals.reportServerPassword}' rsync ${workspaceFolder}/reports-index.html ${globals.reportServerUser}:${reportServerReportPhysicalRoot}/index.html" +
                            "&& sshpass -p '${globals.reportServerPassword}' rsync ${workspaceFolder}/unit-tests-index.html ${globals.reportServerUser}:${reportServerReportPhysicalRoot}/unit-tests/index.html"
                        String publishCoverageReports = "&& dotnet reportgenerator \"-reports:${workspaceFolder}/coverage.xml\" \"-targetdir:${workspaceFolder}/coverage\" " + 
                            "&& sshpass -p '${globals.reportServerPassword}' rsync -r ${workspaceFolder}/coverage ${globals.reportServerUser}:${reportServerReportPhysicalRoot}"

                        if (testProjectFolder != '') {
                            publishCoverageReports = "&& cd ${workspaceFolder}/${testProjectFolder}" +
                                publishCoverageReports +
                                "&& cd ${workspaceFolder}"
                        }

                        if (testDotnet) {
                            testDotnetCommand += " --logger 'trx;LogFileName=../../results.trx'"
                            reportsMap['Unit Test Report'] = 'unit-tests/index.html'
                        }

                        if (coverageReports) {
                            testDotnetCommand += " /p:CollectCoverage=true  /p:CoverletOutputFormat=opencover"
                            reportsMap['Code Coverage Report'] = 'coverage/index.htm'

                            if (coverageDirectory != '') {
                                testDotnetCommand += " /p:CoverletOutputDirectory='${coverageDirectory}'"
                            }
                        }
 
                        // / Reporting variables   
*/
                    }
                    catch (Exception e) {
                        // Check for abort due to master build attempt.
                        if (abortedMasterBuild) {
                            currentBuild.result = 'ABORTED'
                            throw e
                            // return here instead of throwing error to keep the build "green"
                            // return
                        }

                        echo e.toString()
                    }
                }

                echo "Init Stage Complete."
            } // /steps
        } // /stage
        stage('show-job-parameters') {
            steps {               
                echo """
artifactoryPublishRepo: $artifactoryPublishRepo 
artifactoryPublishUrl: $artifactoryPublishUrl
branchName: $branchName
buildNumber: $buildNumber
buildUrl: $buildUrl
buildUrlHtmlMessage: $buildUrlHtmlMessage
cleanWorkspaceAfterRun: $cleanWorkspaceAfterRun
copyClient: $copyClient
copyServer: $copyServer
coverageCommand: $coverageCommand
coverageReports: $coverageReports
dateBuildNumber: $dateBuildNumber
docBuild: $docBuild
docBundleServer: $docBundleServer
fullVersionNumber: $fullVersionNumber
gitChecksum: ${env.GIT_COMMIT}
installDependencies: $installDependencies
installDependenciesCommand: $installDependenciesCommand
isChangeRequest: $isPR
isRelease: $isRel
jobName: $jobName
nodeLabel: $nodeLabel
nodeName: $nodeName
npmArtifactoryUrl: $npmArtifactoryUrl
npmBuild: $npmBuild
npmClean: $npmClean
npmCoverage: $npmCoverage
npmLint: $npmLint
npmPackage: $npmPackage
npmPublish: $npmPublish
npmPublishCommand: $npmPublishCommand
npmRepoName: $npmRepoName
npmScope: $npmScope
npmTest: $npmTest
nugetDeploy: $nugetDeploy
nupkgOctopusActionName: $nupkgOctopusActionName
nupkgOctopusDefaultChannelId: $nupkgOctopusDefaultChannelId
nupkgOctopusProjectId: $nupkgOctopusProjectId
nupkgOctopusStepName: $nupkgOctopusStepName
nupkgOctopusUnstableChannelId: $nupkgOctopusUnstableChannelId
octopusChannelId: $octopusChannelId
packageCommand: $packageCommand
projectFile: $projectFile
projectName: $projectName
reportFolder: $reportFolder
reportsCommandLine: $reportsCommandLine
reportServerPhysicalRoot: $reportServerPhysicalRoot
reportUrl: $reportUrl
runNpmTests: $runNpmTests
runReports: $runReports
slackChannel: $slackChannel
testCommand: $testCommand
testCommandLine: $testCommandLine
testReports: $testReports
version: $version
versionFile: $versionFile
versionSuffix: $versionSuffix
workspaceFolder: $workspaceFolder
"""
            }
        }
        // Install dependencies from the Artifactory NPM registry.
        stage('install-dependencies-npm') {
            when {
                equals expected: true, actual: installDependencies
            }
            steps {
                // Configure npm to use Artifactory.
                // Npm was already configured in the init stage.
                echo "Configured npm to use Artifactory"
                sh "npm install ${installDependenciesCommand}"
                echo "npm install completed"
            }
        }
        // build-npm	npm run build
        stage('build-npm') {
            when {
                equals expected: true, actual: npmBuild
            }
            steps {
                script {                    
                    // Build the application.
                    if (docBuild) {
                        sh 'npm run ci-cd:docBuild'
                    }

                    // Bundling the documentation.
                    if (docBundleServer) {
                        // This developer-specified command copies files to dist/bundle/documentation
                        sh 'npm run ci-cd:docBundleServer'
                    }

                    if (copyServer) {
                        sh 'npm run ci-cd:copyServer'
                    }

                    if (copyClient) {
                        sh 'npm run ci-cd:copyClient'
                    }
                }
            }
        }
        stage('test-npm') { 
            when {
                equals expected: true, actual: runNpmTests
            }
            steps {
                sh "${testCommandLine}"
            }
        }
        // Copy test reports to the reports server.
        stage('copy-reports-npm') { 
            when {
                allOf {
                    equals expected: true, actual: runReports
                    equals expected: false, actual: isPR
                }
            }
            steps {
                sh 'npm run lint'
                sh reportsCommandLine
            }
        }
stage('dist-npm') { steps { echo 'dist-npm' }}
        // Should only run for frameworks
        stage('package-npm') {
            when {
                allOf {
                    equals expected: true, actual: npmPackage
                    equals expected: false, actual: isPR
                }
            }
            steps {
                sh "npm run package${packageCommand}"
            }
        }
        /*
        stage('init') {
        steps {
            script {
                try {
                    String createFolderCommand = "sshpass -p '${reportServerPassword}' ssh -p 22 ${reportServerUser} mkdir -p ${reportServerPhysicalRoot}"
                    String publishTestReports = "&& sshpass -p '${reportServerPassword}' rsync -r reports/ ${reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"
                    String publishCoverageReports = "&& sshpass -p '${reportServerPassword}' rsync -r coverage ${reportServerUser}:${reportServerPhysicalRoot}/${dateBuildNumber}"

                    notificationFooter = "<p>Reports are available at <a href=\'http://${reportServer}/${reportUrl}/\'>View Reports</a></p>"

                    artifactoryZipFileName = "${artifactoryZipFileBaseName}.${fullVersionNumber}.zip"

                    nupkgFileName = "${nugetBaseFileName}.${fullVersionNumber}.nupkg"
                    nuspecFileName = "${nuspecBaseFileName}.${fullVersionNumber}.nuspec"
                    }
                    catch (Exception e) {
                        echo e.toString()
                    }
                }

            echo "Running job with parameters:"
            echo """
artifactoryNugetUrl: $artifactoryNugetUrl
artifactorySourceFolder: $artifactorySourceFolder
artifactoryZipOctopusActionName: $artifactoryZipOctopusActionName
artifactoryZipOctopusProjectId: $artifactoryZipOctopusProjectId
artifactoryZipOctopusStepName: $artifactoryZipOctopusStepName
changeBranch: $changeBranch
changeTarget: $changeTarget
deployArtifacts: $deployArtifacts
deployCommand: $deployCommand
gitBranch: $gitBranch
lintCommand: $lintCommand
notificationFooter: $notificationFooter
nugetBaseFileName: $nugetBaseFileName
nugetPackage: $nugetPackage
nugetPublish: $nugetPublish
nupkgFileName: $nupkgFileName
nuspecAuthor: $nuspecAuthor
nuspecBaseFileName: $nuspecBaseFileName
nuspecFileName: $nuspecFileName
nuspecFolder: $nuspecFolder
octopusApiKey: $globals.octopusApiKey
octopusUrl: $globals.octopusUrl
publishFolder: $publishFolder
reportServer: $globals.reportServer
reportServerPassword: $globals.reportServerPassword
reportServerPhysicalFolder: $reportServerPhysicalFolder
"""
            }
        }
    }
*/
        // publish-npm - publish npms to artifactory.
        stage('publish-npm') {
            when {
                allOf {
                    equals expected: true, actual: npmPublish
                    equals expected: false, actual: isPR
                }
            }
            steps {
                echo "Publishing npms to Artifactory"
                sh 'chmod -R 755 ./scripts'
                sh "npm run publish${npmPublishCommand} --registry https://${npmArtifactoryUrl}"
            }
        }
        // deploy-nuget	- push nuget build to artifactory, make new octopus build pointing to that
        stage('deploy-nuget') {
            when {
                allOf {
                    equals expected: true, actual: nugetDeploy
                    equals expected: false, actual: isPR
                }
            }
            steps {          
                script {                    
                    // Delete any nuspec files from previous runs.
                    sh 'rm -rf ' + docFilesFolder + '/*.nuspec'

                    // Define the nuspec file name for the simulated NuGet package.
                    String nuspecFileName = "CAF.Angular.Documentation.${fullVersionNumber}.nuspec"
                    echo "Writing nuspec file: ${docFilesFolder}/${nuspecFileName}"
                    writeNuspecFile docFilesFolder, nuspecFileName, 'CAF.Angular.Documentation', fullVersionNumber, 'CAF Team', 'Common Application Framework Angular Documentation App'

                    // Delete any nupkg files from previous runs.
                    sh 'rm -rf *.nupkg'
                    // Define the nupkg file name.
                    String nupkgFileName = "CAF.Angular.Documentation.${fullVersionNumber}.nupkg"

                    // Move to the docFilesFolder folder and create the nupkg file in the workspace folder using zip.
                    sh "cd ${docFilesFolder} && zip -r ${workspaceFolder}/${nupkgFileName} ." 

                    echo "Publish documentation app nuget to Artifactory"
                    sh "dotnet nuget push ${nupkgFileName} --api-key ${globals.artifactoryUserName}:${globals.artifactoryNugetPassword} --source https://${artifactoryPublishDocsUrl}"

                    echo "Creating new Octopus Release which will trigger auto-deploy"
                    // Create the JSON file and save it in the current folder.
                    createOctopusRelease(
                        projectId: nupkgOctopusProjectId, 
                        version: fullVersionNumber, 
                        stepName: nupkgOctopusStepName, 
                        actionName: nupkgOctopusActionName, 
                        octopusChannelId: octopusChannelId,
                        buildUrl: buildUrl
                    )
                } 
           }
        }
    }
    post {
            // Send notifications after the job completes.
            success {
                emailext (
                recipientProviders: [[$class: 'DevelopersRecipientProvider']],
                to: "${notified}",
                mimeType: 'text/html',
                subject: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                slackSend channel: slackChannel, color: "#00FF00", message: "SUCCESSFUL: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}${notificationSlackFooter}"
            }
            failure {
                emailext (
                recipientProviders: [[$class: 'CulpritsRecipientProvider']],
                to: "${notified}",
                mimeType: 'text/html',
                subject: "ERROR: Job ${jobName} [${fullVersionNumber}]",
                body: "<p>ERROR: Job ${jobName} [${fullVersionNumber}]:</p>${buildUrlHtmlMessage}${notificationHtmlFooter}"
                )
                slackSend channel: slackChannel, color: "#FF0000", message: "ERROR: Job ${jobName} [${fullVersionNumber}]${buildUrlSlackMessage}${notificationSlackFooter}"
            }
        }
    }
}
